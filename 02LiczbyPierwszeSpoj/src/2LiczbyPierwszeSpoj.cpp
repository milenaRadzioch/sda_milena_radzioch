#include <iostream>
using namespace std;

bool sprawdzCzyPierwsza(int n)
{
	if (n <= 1)
	{
		return false;
	}

	for (int i = 2; i <= n / 2; i++)
	{
		if (n % i == 0)
		{
			return false;
		}
	}
	return true;
}

int main()
{
	int x;
	cin >> x;

	if (sprawdzCzyPierwsza(x))
	{
		cout << "TAK" << endl;
	}
	else
	{
		cout << "NIE" << endl;
	}

	return 0;
}
