//Zmienne pomocnicze
//n - rozmiar tablicy
//j - indeks aktualnego elementu
//x - tymczasowa warto�(elementu sortowanego)
//i - indeks nast�pnego elementu (przeszukuje elementy posortowane)
int n;
int tab[n]; //tablica n-elementowa
int x; //Deklaracja zmiennej pomocniczej

for (int j = n-1; j >= 0; j--) //P�tla g��wna, iteruj�ca po wszystkich elementach tablicy
{
	x = tab[j]; //Zapisanie wartosci do zmiennej tymczasowej
	i = j+1; // Ustawienie indeksu na "posortowan� tablic�"

	while( (i<n) && (x > tab[i]) ) //Sprawdzenie czy nie jest to ostatni indeks i czy element�w nie nale�y ze sob� zamieni�
	{
		tab[i-1] = tab[i]; //Nadpisanie elementu (przesuwanie)
		i++; //Przejscie do nast�pnego elemetnu
	}
	tab[i-1] = x; //Wstawienie sortowanej warto�ci na "wolne" miejsce
}
