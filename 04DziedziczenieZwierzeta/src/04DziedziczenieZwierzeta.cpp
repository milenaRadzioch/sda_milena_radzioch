#include <iostream>

class Zwierze
{
public:
	void dajGlos()
	{
		std::cout << "Jestem zwierzem" << std::endl;
	}
};

class Ogoniaste
{
public:
	void jakiOgon()
	{
		std::cout << "Mam ogon!" << std::endl;
	}
};

class Ssak: public Zwierze
{
public:
	void ileMamNog()
	{
		std::cout << "Nie wiem ile mam n�g" << std::endl;
	}
};

class Pies: public Ssak, public Ogoniaste
{
};

int main()
{
	Zwierze zwierze;
	zwierze.dajGlos();
//	zwierze.ileMamNog();

	Ssak ssak;
	ssak.dajGlos();
	ssak.ileMamNog();

	Pies pies;
	pies.dajGlos();
	pies.ileMamNog();
	pies.jakiOgon();

	return 0;
}
