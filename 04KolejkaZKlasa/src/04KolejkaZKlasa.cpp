#include <iostream>

const int MAKSYMALNY_ROZMIAR_KOLEJKI = 5;

class Kolejka
{
private:
	int dane[MAKSYMALNY_ROZMIAR_KOLEJKI];
	int koniec;

public:
	Kolejka()
	{
		koniec = 0;
	}

	void pokazKolejke()
	{
		std::cout << koniec << " [";

		for (int i = 0; i < koniec; ++i)
		{
			std::cout << dane[i] << ' ';
		}

		std::cout << "]" << std::endl;
	}

	void wstaw(int nowaWartosc)
	{
		if (koniec >= MAKSYMALNY_ROZMIAR_KOLEJKI)
		{
			std::cerr << "Kolejka pe�na" << std::endl;
			return;
		}
		// nowa osoba do kolejki
		dane[koniec] = nowaWartosc;
		koniec += 1;
	}

	int usun()
	{
		// usu� osob� z kolejki
		if (koniec > 0)
		{
			int obsluzony = dane[0];
			for (int i = 1; i < koniec; ++i)
			{
				dane[i - 1] = dane[i];
			}
			koniec -= 1;
			return obsluzony;
		}
		std::cerr << "Kolejka pusta" << std::endl;
		return -1;
	}

};

int main()
{
	// stworzenie nowej pustej kolejki kolejki
	Kolejka naNarty;
	naNarty.pokazKolejke();
	naNarty.wstaw(90);
	naNarty.pokazKolejke();
	naNarty.wstaw(76);
	naNarty.pokazKolejke();
	naNarty.wstaw(54);
	naNarty.pokazKolejke();

	int usuniete = naNarty.usun();
	std::cout << "Usuni�te: " << usuniete << std::endl;

	naNarty.pokazKolejke();
	naNarty.wstaw(32);
	naNarty.pokazKolejke();
	naNarty.usun();
	naNarty.pokazKolejke();
	naNarty.usun();
	naNarty.pokazKolejke();
	naNarty.usun();
	naNarty.pokazKolejke();

	return 0;
}
