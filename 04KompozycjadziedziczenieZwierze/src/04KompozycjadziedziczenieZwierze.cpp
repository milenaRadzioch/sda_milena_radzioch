//============================================================================
// Name        : 04KompozycjadziedziczenieZwierze.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

class Latanie
{
public:
	virtual ~Latanie() {}

	virtual void jakLata()
	{
	}
};

class Zwierze
{
public:
	Latanie* latanie;

	virtual ~Zwierze()
	{
	}
};

class Ssak: public Zwierze
{
public:
	virtual ~Ssak()
	{
	}
};

class NaSkrzydlach: public Latanie
{
public:
	void jakLata()
	{
		std::cout << "Na skrzydłach" << std::endl;
	}
};

class Nielot: public Latanie
{
public:
	void jakLata()
	{
		std::cout << "Nie lata :(" << std::endl;
	}
};

int main()
{
	Ssak czlowiek;
	czlowiek.latanie = new Nielot();
	Ssak nietoperz;
	nietoperz.latanie = new NaSkrzydlach();

	Ssak* zoo[2];
	zoo[0] = &czlowiek;
	zoo[1] = &nietoperz;

	for (int i = 0; i < 2; ++i)
	{
		zoo[i]->latanie->jakLata();
	}

	delete czlowiek.latanie;
	delete nietoperz.latanie;
	return 0;
}
