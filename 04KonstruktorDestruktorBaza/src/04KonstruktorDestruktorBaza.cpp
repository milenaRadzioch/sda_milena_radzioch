#include <iostream>

class A
{
public:
	A(int x)
	{
		std::cout << "Konstruktor A #1" << std::endl;
	}
	A(int x, int y)
	{
		std::cout << "Konstruktor A #2" << std::endl;
	}
	~A()
	{
		std::cout << "Destruktor A" << std::endl;
	}
};

class B: public A
{
public:
	B(int y, int z) :
			A(y)
	{
		std::cout << "Konstruktor B #1" << std::endl;
	}
	B(int a) :
			A(a, 0)
	{
		std::cout << "Konstruktor B #2" << std::endl;
	}
	~B()
	{
		std::cout << "Destruktor B" << std::endl;
	}
};

class C
{
public:
	C()
	{
		std::cout << "Konstruktor C" << std::endl;
	}
	~C()
	{
		std::cout << "Destruktor C" << std::endl;
	}
};

class D: public C, public B
{
public:
	D() :
			C(), B(0, 0)
	{
		std::cout << "Konstruktor D" << std::endl;
	}
	~D()
	{
		std::cout << "Destruktor D" << std::endl;
	}
};

void separator()
{
	std::cout << "-----------------" << std::endl;
}

int main()
{
	separator();
	A a(1, 2);
	separator();
	B b(2);
	separator();
	C c;
	separator();
	D d;
	separator();

	return 0;
}
