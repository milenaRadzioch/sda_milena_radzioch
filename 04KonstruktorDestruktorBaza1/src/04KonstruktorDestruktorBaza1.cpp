#include <iostream>

class Foo
{
private:
	int x;

public:
	Foo()
	{
		x = 0;
		std::cout << "Konstruktor Foo bezargumentowy" << std::endl;
	}

	Foo(int nowaWartosc)
	{
		x = nowaWartosc;
		std::cout << "Konstruktor Foo z argumentem" << std::endl;
	}

	~Foo()
	{
		std::cout << "Destruktor Foo" << std::endl;
	}

	void czesc()
	{
		std::cout << "Hello, hello! x = " << x << std::endl;
	}

	int getX()
	{
		return x;
	}
};

void podprogram()
{
	Foo obiekt2;
	obiekt2.czesc();
	std::cout << "Hello!" << std::endl;
}

int main()
{
//	std::cout << "Podaj liczbe: " << std::flush;
	int wczytana = 99;
//	std::cin >> wczytana;

	std::cout << "Przed stworzeniem" << std::endl;
	Foo obiekt1(wczytana);
	std::cout << "Po stworzeniu" << std::endl;
	obiekt1.czesc();
	obiekt1.czesc();
	std::cout << "x=" << obiekt1.getX() << std::endl;

	std::cout << "--------------------------" << std::endl;
	podprogram();
	std::cout << "--------------------------" << std::endl;

	return 0;
}
