#include <iostream>
#include <string>

struct Ksiazka
{
	std::string tytul;
	std::string autor;
	int iloscStron;
	double cena;
};

void wypiszKsiazke(Ksiazka ksiazka)
{
	std::cout << "Tytu�: " << ksiazka.tytul << std::endl;
	std::cout << "Autor: " << ksiazka.autor << std::endl;
	std::cout << "Ilo�� stron: " << ksiazka.iloscStron << std::endl;
	std::cout << "Cena: " << ksiazka.cena << std::endl;
}

void stworzKsiazke(Ksiazka& nowa, std::string tytul2, std::string autor2,
		int iloscStron2, double cena2)
{
	nowa.tytul = tytul2;
	nowa.autor = autor2;
	nowa.iloscStron = iloscStron2;
	nowa.cena = cena2;
}

int main()
{
	const int ILOSC_KSIAZEK = 5;
	Ksiazka ksiazki[ILOSC_KSIAZEK];

	stworzKsiazke(ksiazki[0], "Hobbit, czyli tam i z powrotem",
			"J.R.R. Tolkien", 300, 25.0);
	stworzKsiazke(ksiazki[1], "Dru�yna pier�cienia", "J.R.R. Tolkien", 500,
			40.0);
	stworzKsiazke(ksiazki[2], "Kroniki Amberu", "Roger Zelazny", 550, 40.0);
	stworzKsiazke(ksiazki[3], "Diuna", "F. Herbert", 600, 50.5);
	stworzKsiazke(ksiazki[4], "Kroniki Jakuba W�drowycza", "Andrzej Pilipiuk",
			350, 36.25);

	for (int i = 0; i < ILOSC_KSIAZEK; ++i)
	{
		wypiszKsiazke(ksiazki[i]);
	}

	return 0;
}
