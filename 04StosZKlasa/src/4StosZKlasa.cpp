#include <iostream>

const int MAKSYMALNY_ROZMIAR_STOSU = 5;

class Stos
{
private:
	int wierzcholek; // pozycja w tablicy "dane" - gdzie zapisa� nowy element
	int dane[MAKSYMALNY_ROZMIAR_STOSU];

public:
	Stos()
	{
		wierzcholek = 0;
		for (int i = 0; i < MAKSYMALNY_ROZMIAR_STOSU; ++i)
		{
			dane[i] = 0;
		}
	}

	int jakiRozmiar()
	{
		return wierzcholek;
	}

	bool pusty()
	{
		return jakiRozmiar() == 0;
	}

	bool pelny()
	{
		return jakiRozmiar() >= MAKSYMALNY_ROZMIAR_STOSU;
	}

	int jakiWierzcholek()
	{
		// sprawd�, co jest na wierzchu stosu
		// je�li stos jest pusty, to poinformuj o tym
		if (pusty())
		{
			std::cerr << "Stos jest pusty!" << std::endl;
			return -1; // sytuacja wyj�tkowa! zwracam "�mieciow�" warto��. tu powinien by� wyj�tek
		}

		// w przeciwnym wypadku - poka� wierzcho�ek
		// ostatni wstawiony element znajduje si� o 1 ni�ej ni� miejsce na nowy element
		int wierzch = dane[wierzcholek - 1];
		return wierzch;
	}

	void wstaw(int nowaWartosc)
	{
		if (pelny())
		{
			std::cerr << "Stos pe�ny!" << std::endl;
			return; // sytuacja wyj�tkowa! tu powinien by� wyj�tek
		}

		// wstawi� liczb� na szczyt stosu
		dane[wierzcholek] = nowaWartosc; // zapisz dane na wierzcho�ku stosu
		wierzcholek += 1;			 			// przesuwam wierzcho�ek
	}

	int zdejmij()
	{
		if (pusty())
		{
			std::cerr << "Pr�ba zdj�cia z pustego stosu!" << std::endl;
			return -1; // sytuacja wyj�tkowa! zwracam "�mieciow�" warto��. tu powinien by� wyj�tek
		}

		wierzcholek -= 1; // przesuwam wierzcho�ek na pierwszy istniej�cy element
		int zdjete = dane[wierzcholek]; // odczytaj dane z wierzchu stosu
		return zdjete;
	}

};

int main()
{
	// nowy stos
	Stos liczby;
	std::cout << "Rozmiar: " << liczby.jakiRozmiar() << std::endl;

	liczby.wstaw(4);
	std::cout << "Na g�rze jest: " << liczby.jakiWierzcholek() << std::endl;
	std::cout << "Rozmiar: " << liczby.jakiRozmiar() << std::endl;

	liczby.wstaw(7);
	std::cout << "Na g�rze jest: " << liczby.jakiWierzcholek() << std::endl;
	std::cout << "Rozmiar: " << liczby.jakiRozmiar() << std::endl;

	int zdjete1 = liczby.zdejmij();
	std::cout << "Zdj�te: " << zdjete1 << std::endl;
	std::cout << "Na g�rze jest: " << liczby.jakiWierzcholek() << std::endl;
		std::cout << "Rozmiar: " << liczby.jakiRozmiar() << std::endl;

	int zdjete2 = liczby.zdejmij();
	std::cout << "Zdj�te: " << zdjete2 << std::endl;
	std::cout << "Na g�rze jest: " << liczby.jakiWierzcholek() << std::endl;
	std::cout << "Rozmiar: " << liczby.jakiRozmiar() << std::endl;

	int zdjete3 = liczby.zdejmij();
	std::cout << "Zdj�te: " << zdjete3 << std::endl;

	liczby.wstaw(42);
	liczby.wstaw(128);
	liczby.wstaw(-1);
	liczby.wstaw(8);
	liczby.wstaw(-4);
	liczby.wstaw(10);
	liczby.wstaw(100);
	std::cout << "Rozmiar: " << liczby.jakiRozmiar() << std::endl;
	std::cout << "Na g�rze jest: " << liczby.jakiWierzcholek() << std::endl;
	liczby.wstaw(100);

	return 0;
}
