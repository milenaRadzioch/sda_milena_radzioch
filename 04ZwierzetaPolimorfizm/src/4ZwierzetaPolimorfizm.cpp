#include <iostream>

class Zwierze
{
public:
	virtual ~Zwierze()
	{
	}

	virtual void dajGlos()
	{
		std::cout << "Jestem zwierzem" << std::endl;
	}
};

class Ogoniaste
{
public:
	void jakiOgon()
	{
		std::cout << "Mam ogon!" << std::endl;
	}
};

class Ssak: public Zwierze
{
public:
	virtual ~Ssak()
	{
	}

	virtual void ileMamNog()
	{
		std::cout << "Nie wiem ile mam n�g" << std::endl;
	}
};

class Pies: public Ssak, public Ogoniaste
{
public:
	virtual ~Pies()
	{
	}

	void dajGlos()
	{
		std::cout << "Jestem psem" << std::endl;
	}
};

class Czlowiek: public Ssak
{
public:
	virtual ~Czlowiek()
	{
	}

	void dajGlos()
	{
		std::cout << "Jestem czlowiekiem!" << std::endl;
	}
	void ileMamNog()
	{
		std::cout << "Mam dwie nogi" << std::endl;
	}
};

// Funkcja patrzy na osobnika przez "pryzmat" Zwierz�cia,
// ale to obiekt, kt�ry faktycznie przeka�emy dostarczy implementacj�
void zaprezentujZwierze(Zwierze* osobnik)
{
	osobnik->dajGlos();
}

int main()
{
	Zwierze zwierze;
	zwierze.dajGlos();
//	zwierze.ileMamNog(); -- Zwierze nie ma tej metody

	Ssak ssak;
	ssak.dajGlos();
	ssak.ileMamNog();

	Pies pies;
	pies.dajGlos();
	pies.ileMamNog();
	pies.jakiOgon();

	Czlowiek czlowiek;

	Zwierze* zoo[4];
	zoo[0] = &zwierze;
	zoo[1] = &ssak;
	zoo[2] = &pies;
	zoo[3] = &czlowiek;

	Ssak* schronisko[3];
//	schronisko[0] = &zwierze -- Zwierze nie jest Ssakiem
	schronisko[0] = &ssak;
	schronisko[1] = &pies;
	schronisko[2] = &czlowiek;

	std::cout << "-- Zoo --------------------" << std::endl;
	for (int i = 0; i < 4; ++i)
	{
		zaprezentujZwierze(zoo[i]);
	}

	std::cout << "-- Schronisko ---------------------" << std::endl;
	for (int i = 0; i < 3; ++i)
	{
		schronisko[i]->ileMamNog();
	}
	return 0;
}
