#include <iostream>
using namespace std;

int fib(int n)
{
	if (0 == n)
	{
		return 0;
	}
	else if (1 == n)
	{
		return 1;
	}
	else
	{
		return fib(n - 1) + fib(n - 2);
	}
}
int main()
{
	int n;
	cout << "Podaj liczbe z ci�gu Fibonacci: " << flush;
	cin >> n;
	cout << fib(n);
}
