#include <iostream>
#include <cstdlib>
#include <cerrno>

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		std::clog << "Za ma�o argument�w!" << std::endl;
		return 1;
	}
	else if (argc > 3)
	{
		std::clog << "Za du�o argument�w!" << std::endl;
		return 1;
	}

	char* arg1end;
	long x = std::strtol(argv[1], &arg1end, 0);
	if (arg1end[0] != '\0')
	{
		std::clog << "Argument #1 nie jest liczb�!" << std::endl;
		return 1;
	}
	else if (errno == ERANGE)
	{
		std::clog << "Argument #1 nie zmie�ci� si� w zmiennej" << std::endl;
		return 1;
	}

	char* arg2end;
	long y = std::strtol(argv[2], &arg2end, 0);
	if (arg2end[0] != '\0')
	{
		std::clog << "Argument #2 nie jest liczb�!" << std::endl;
		return 1;
	}
	else if (errno == ERANGE)
	{
		std::clog << "Argument #2 nie mo�e by� zapisany w zakresie"
				<< std::endl;
		return 1;
	}

	std::cout << "Program: " << argv[0] << std::endl;
	std::cout << "Argumenty: x = " << x << ", y = " << y << std::endl;

	while (true)
	{
		std::cout << " Kalkulator" << std::endl << "============" << std::endl
				<< " 1. Dodaj" << std::endl << " 2. Odejmij" << std::endl
				<< " 3. Pomn�" << std::endl;
		if (y != 0)
		{
			std::cout << " 4. Podziel" << std::endl << " 5. Reszta z dzielenia"
					<< std::endl;
		}
		std::cout << " 0. Wyjd�" << std::endl;

		char wybor;
		std::cin >> wybor;

		switch (wybor)
		{
		case '1':
			std::cout << "SUMA: " << x + y << std::endl;
			break;
		case '2':
			std::cout << "RӯNICA: " << x - y << std::endl;
			break;
		case '3':
			std::cout << "ILOCZYN: " << x * y << std::endl;
			break;
		case '4':
			if (y == 0)
			{
				std::clog << "Nie dziel przez 0!" << std::endl;
				continue;
			}
			std::cout << "ILORAZ: " << x / y << std::endl;
			break;
		case '5':
			if (y == 0)
			{
				std::clog << "Nie dziel przez 0!" << std::endl;
				continue;
			}
			std::cout << "MODULO: " << x % y << std::endl;
			break;
		case '0':
			std::cout << "\nKoniec!" << std::endl;
			return 0;
		default:
			std::clog << "Nieprawid�owy wyb�r!" << std::endl;
		}
	}

	return 0;
}
