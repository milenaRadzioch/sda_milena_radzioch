#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
	std::string rzym;
	std::getline(std::cin, rzym);

	if (rzym.empty())
	{
		std::cout << "Koniec" << std::endl;
		return 0;
	}
	int wynik = 0;
	for (unsigned i = 0; i < rzym.length(); ++i)
	{
		char znak = rzym[i];
		char nastepnyZnak = (i + 1 < rzym.length()) ? rzym[i + 1] : '\0';
		if (znak == 'I' && nastepnyZnak == 'V')
		{
			wynik = wynik - 1;
		}
		else if (znak == 'I' && nastepnyZnak == 'X')
		{
			wynik = wynik - 1;
		}
		else if (znak == 'I')
		{
			wynik = wynik + 1;
		}
		else if (znak == 'V')
		{
			wynik = wynik + 5;
		}
		else if (znak == 'X' && nastepnyZnak == 'L')
		{
			wynik = wynik - 10;
		}
		else if (znak == 'X' && nastepnyZnak == 'C')
		{
			wynik = wynik - 10;
		}
		else if (znak == 'X')
		{
			wynik = wynik + 10;
		}
		else if (znak == 'L')
		{
			wynik = wynik + 50;
		}
		else if (znak == 'C' && nastepnyZnak == 'D')
		{
			wynik = wynik - 100;
		}
		else if (znak == 'C' && nastepnyZnak == 'M')
		{
			wynik = wynik - 100;
		}
		else if (znak == 'C')
		{
			wynik = wynik + 100;
		}
		else if (znak == 'D')
		{
			wynik = wynik + 500;
		}
		else if (znak == 'M')
		{
			wynik = wynik + 1000;
		}
	}
	std::cout << wynik << std::endl;

	return 0;
}
