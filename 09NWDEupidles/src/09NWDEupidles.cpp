#include <iostream>
#include <cmath>
using namespace std;

int m, n, r;

void NWD(int m, int n)
{
	if (m == 0)
	{
		cout << n << endl;
		return;
	}
	else
	{
		r = n % m;
		NWD(r, m);
	}
}

int main()
{
	int m, n;
	cout << "Podaj argument m: " << endl;
	cin >> m;
	cout << "Podaj argument n: " << endl;
	cin >> n;
	cout << "Najwiekszy wsp�lny dzielnik: " << endl;
	NWD(m, n);

	return 0;
}
