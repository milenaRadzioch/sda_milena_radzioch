#include <iostream>

int main()
{

	using namespace std;

	int a;
	int b;

	cout << "Kalkulator" << endl;
	cout << "Podaj warto�� liczby a : " << endl;
	cin >> a;
	cout << "Podaj warto�� liczby b : " << endl;
	cin >> b;

	cout << a << " + " << b << " = " << (a + b) << endl;
	cout << a << " - " << b << " = " << (a - b) << endl;
	cout << a << " * " << b << " = " << (a * b) << endl;
	cout << a << " / " << b << " = " << (a / b) << endl;
	cout << a << " % " << b << " = " << (a % b) << endl;
	cout << a << " ~ " << " = " << (~a) << endl;
	cout << a << " & " << b << " = " << (a & b) << endl;
	cout << a << " | " << b << " = " << (a | b) << endl;
	cout << a << " ^ " << b << " = " << (a ^ b) << endl;
	cout << a << " >> " << b << " = " << (a >> b) << endl;
	cout << a << " << " << b << " = " << (a << b) << endl;
	cout << a << " || " << b << " = " << (a || b) << endl;
	cout << a << " && " << b << " = " << (a && b) << endl;
	cout << a << " < " << b << " = " << (a < b) << endl;
	cout << a << " > " << b << " = " << (a > b) << endl;
	cout << a << " == " << b << " = " << (a == b) << endl;

	cout << " ++a " << " = " << (++a) << endl;
	cout << " ++b " << " = " << (++b) << endl;
	cout << " --a " << " = " << (--a) << endl;
	cout << " --b " << " = " << (--b) << endl;

	return 0;
}
