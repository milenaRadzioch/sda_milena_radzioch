#include <iostream>

int main()
{
	int a, b;
	std::cout << "A=" << std::flush;
	std::cin >> a;
	std::cout << "B=" << std::flush;
	std::cin >> b;

	std::cout << "\na+b=" << a+b
	 		  << "\na-b=" << a-b
	 		  << "\na*b=" << a*b
	 		  << "\na/b=" << a/b
	 		  << "\na%b=" << a%b
	 		  << "\na&&b=" << (a&&b)
	 		  << "\na||b=" << (a||b)
	 		  << "\na&b=" << (a&b)
	 		  << "\na|b=" << (a|b)
	 		  << "\na|~b=" << (a|~b)
	 		  << "\n!a|b=" << (!a|b) // uwaga na b��d! mix logiczny i bitowy
	 		  << "\na^b=" << (a^b)
	 		  << "\na||!b=" << (a||!b)
	 		  << "\na>>b=" << (a>>b)
	 		  << "\na<<b=" << (a<<b)
	 		  << "\na+++b=" << (a+++b)
	 		  << "\na>b=" << (a>b)
	 		  << "\na<b=" << (a<b)
	 		  << "\na==b=" << (a==b)
	 		  << "\nsizeof(a)=" << sizeof(a)
			  << std::endl;

	return 0;
}
