#include <iostream>

int main()
{
	std::cout << "Podaj ilo�� liczb: " << std::endl;
	int ilosc;
	std::cin >> ilosc;

	// dynamiczna tablica o rozmiarze ilosc
	int* t = new int[ilosc];

	// wczytanie liczb do tej tablicy

	for (int i = 0; i < ilosc; i++)
	{
		std::cout << "Podaj " << i + 1 << ". liczb�: " << std::flush;
		std::cin >> t[i];
	}

	// obliczenie sumy elementow tablicy
	int wynik = 0;
	for (int i = 0; i < ilosc; i++)
	{
		wynik += t[i];
	}

	// wyswietlenie wyniku
	std::cout << "Suma element�w tablicy to: " << wynik << std::flush;

	// zwolnienie tablicy
	delete[] t;
	return 0;
}
