#include <iostream>

int main()
{
	int a = 10;
	int* x = NULL;

	std::cout << "A=" << a << std::endl;
	std::cout << "X=" << x << std::endl;

	std::cout << "&A=" << (&a) << std::endl;
	std::cout << "&X=" << (&x) << std::endl;

	x = &a;
	std::cout << "X=" << x << std::endl;
	std::cout << "&X=" << (&x) << std::endl;

	std::cout << "A=" << a << std::endl;
	std::cout << "*X=" << (*x) << std::endl;

	a += 1;

	std::cout << "A=" << a << std::endl;
	std::cout << "*X=" << (*x) << std::endl;

	*x += 5;

	std::cout << "A=" << a << std::endl;
	std::cout << "*X=" << (*x) << std::endl;

	std::cout << "&X=" << (&x) << std::endl;

	int* y = NULL;
	int* z = NULL;

	std::cout << "Y=" << y << std::endl;
	std::cout << "&Y=" << (&y) << std::endl;
	std::cout << "Z=" << z << std::endl;
	std::cout << "&Z=" << (&z) << std::endl;

	y = new int;
	z = new int;

	std::cout << "Y=" << y << std::endl;
	std::cout << "&Y=" << (&y) << std::endl;
	std::cout << "Z=" << z << std::endl;
	std::cout << "&Z=" << (&z) << std::endl;

	delete y;
	delete z;

	std::cout << "Y=" << y << std::endl;
	std::cout << "Z=" << z << std::endl;

	int* t = NULL;

	int rozmiar;
	std::cout << "Podaj rozmiar (wi�kszy albo r�wny 3): " << std::flush;
	std::cin >> rozmiar;

	t = new int[rozmiar];

	t[0] = 5;
	t[1] = 10;
	t[2] = 15;

	std::cout << "T=" << t << std::endl;
	std::cout << "*(T+0)=" << *(t + 0) << std::endl;
	std::cout << "T[0]=" << t[0] << std::endl;
	std::cout << "*(T+1)=" << *(t + 1) << std::endl;
	std::cout << "T[1]=" << t[1] << std::endl;
	std::cout << "*(T+2)=" << *(t + 2) << std::endl;
	std::cout << "T[2]=" << t[2] << std::endl;

	delete[] t;

	return 0;
}
