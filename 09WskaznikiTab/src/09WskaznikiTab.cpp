#include <iostream>

int main2(int argc, char* argv[])
{
	int tab[3] = { 3, 4, 5 };
	std::cout << "sizeof(int) = " << sizeof(int) << std::endl;
	std::cout << "sizeof(tab) = " << sizeof(tab) << std::endl;

//	std::cout << "  tab    =" << tab << std::endl;
//	std::cout << "& tab    =" << &tab << std::endl;
//	std::cout << "  tab[0] =" << tab[0] << std::endl;
//	std::cout << "&(tab[0])=" << &(tab[0]) << std::endl;
//	std::cout << "* tab    =" << *tab << std::endl;
//	std::cout << "&(*tab)  =" << &(*tab) << std::endl;
//	std::cout << "&(tab[1])=" << &(tab[1]) << std::endl;

	double* dbls = new double[4];
	std::cout << "sizeof(double) = " << sizeof(double) << std::endl;
	std::cout << "sizeof(dbls)   = " << sizeof(dbls) << std::endl;

//	std::cout << "  dbls    =" << dbls << std::endl;
//	std::cout << "& dbls    =" << &dbls << std::endl;
//	std::cout << "  dbls[0] =" << dbls[0] << std::endl;
//	std::cout << "&(dbls[0])=" << &(dbls[0]) << std::endl;
//	std::cout << "* dbls    =" << *dbls << std::endl;
//	std::cout << "&(*dbls)  =" << &(*dbls) << std::endl;
//	std::cout << "&(dbls[1])=" << &(dbls[1]) << std::endl;

	delete[] dbls;

	std::cout << "---------------" << std::endl;
	int* t = tab;
	std::cout << "sizeof(t) = " << sizeof(t) << std::endl;

//	std::cout << "  t    =" << t << std::endl;
//	std::cout << "& t    =" << &t << std::endl;
//	std::cout << "  t[0] =" << t[0] << std::endl;
//	std::cout << "&(t[0])=" << &(t[0]) << std::endl;
//	std::cout << "* t    =" << *t << std::endl;
//	std::cout << "*(t+0) =" << *(t + 0) << std::endl;
//	std::cout << "&(*t)  =" << &(*t) << std::endl;
//	std::cout << "&(t[1])=" << &(t[1]) << std::endl;
//	std::cout << "  t[1] =" << t[1] << std::endl;
//	std::cout << "*(t+1) =" << *(t + 1) << std::endl;

//	t += 1;
//	std::cout << "  t    =" << t << std::endl;
//	std::cout << "* t    =" << *t << std::endl;

	return 0;
}
