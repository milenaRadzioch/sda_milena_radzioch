#include <iostream>
using namespace std;

void szescian(double x, double* wynik)
{
	*wynik = x * x * x;
}

int main()
{

	cout << "Podaj liczbe: " << endl;
	double liczba;
	cin >> liczba;

	double wynik;
	szescian(liczba, &wynik);

	cout << "Sze�cian liczby : " << wynik << endl;

	return 0;
}
