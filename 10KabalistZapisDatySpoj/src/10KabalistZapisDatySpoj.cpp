#include <iostream>
#include <string>
using namespace std;

int main()
{
	char znaki[] =
	{ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'p',
			'q', 'r', 's', 't', 'v', 'x', 'y', 'z' };
	int wartosc[] =
	{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200,
			300, 400, 500 };

	string wyraz;
	cin >> wyraz;
//	wyraz = "info";
	int rok = 0;

	for (unsigned int i = 0; i < wyraz.size(); i++)
	{
		for (int k = 0; k < 23; k++)
		{
			if (znaki[k] == wyraz[i])
			{
				rok += wartosc[k];
				break;
			}
		}
	}
	cout << rok << endl;

	return 0;
}
