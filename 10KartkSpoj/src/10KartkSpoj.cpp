#include <iostream>
using namespace std;

int main()
{
	long n;
	cout << "podaj n: ";
	cin >> n;

	long zero = 0;
	for (long i = n / 5; i > 0; i = i / 5)
	{
		zero += i;
	}

	cout << "ilosc zer na koncu " << n << "! wynosi " << zero << endl;

	return 0;
}
