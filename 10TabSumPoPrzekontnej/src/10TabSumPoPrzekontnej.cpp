#include <iostream>
#include <cmath>
using namespace std;

int main()
{
	int marks[4][4] =
	{
	{ 50, 60, 55, 67 },
	{ 62, 65, 70, 81 },
	{ 66, 77, 80, 69 },
	{ 72, 77, 80, 69 } };

	int sumUp = 0;
	int sumDown = 0;

	for (int y = 0; y < 4; y++)
	{

		for (int x = 0; x < 4; x++)
		{
			if (x < y)
			{
				sumDown = sumDown + marks[y][x];
			}
			else if (x > y)
			{
				sumUp = sumUp + marks[y][x];
			}

		}
	}
	cout << "suma nad " << sumUp << endl;
	cout << "suma pod " << sumDown << endl;

	return 0;
}
