//============================================================================
// Name        : 10Zad)rozgrzewka.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int foo(int nValue)
{
	if (nValue == 0)
	{
		return 999;
	}
	else
	{
		return nValue;
	}
}

int main()
{
	std::cout << foo(0) << std::endl;
	std::cout << foo(1) << std::endl;
	std::cout << foo(2) << std::endl;

	return 0;
}
