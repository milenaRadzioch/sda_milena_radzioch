#include <iostream>

float podziel(float x, float y)
{
	return x / y;
}

int main()
{
	int nX = 7;
	int nY = 2;

	std::cout << podziel(nX, nY) << std::endl;

	return 0;
}
