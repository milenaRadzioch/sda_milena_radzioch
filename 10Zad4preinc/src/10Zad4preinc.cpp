#include <iostream>
using namespace std;

int mnoz(int x, int y)
{
	return x * y;
}

int main()
{
	int x = 5;

	cout << mnoz(x, x++) << endl;

	return 0;
}
