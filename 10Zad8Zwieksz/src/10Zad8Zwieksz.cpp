#include <iostream>
using namespace std;

void foo(int a, int b)
{
	cout << "a: " << a << endl;
	cout << "b: " << b << endl;
}

int main()
{
	int i = 1;
	{
		foo(i, ++i);
	}
	return 0;
}
