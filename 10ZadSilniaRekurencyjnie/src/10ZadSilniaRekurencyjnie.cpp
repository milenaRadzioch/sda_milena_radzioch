#include<iostream>
using namespace std;

long silnia(int n);

int main()
{
	int n(0);
	cin >> n;
	long val = silnia(n);
	cout << val << std::endl;
	cin.get();
	return 0;
}

long silnia(int n)
{
	long result(n);
	while (n-- && n != 0)
	{
		result *= n;
	}
	return result;
}
