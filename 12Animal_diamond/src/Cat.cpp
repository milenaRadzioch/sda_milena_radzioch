
#include <string>
#include <iostream>

#include "Cat.hpp"

#include "WildAnimal.hpp"

Cat::Cat()
:WildAnimal("")
{
}

Cat::Cat(std::string nameCat)
:WildAnimal(nameCat)
{
}

void Cat::giveVoice() {
	std::cout << mNameAnimal << " meow meow "<<std::endl;
}

Cat::~Cat() {

}

