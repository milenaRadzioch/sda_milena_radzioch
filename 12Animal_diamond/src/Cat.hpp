
#include <string>
#include "WildAnimal.hpp"

#ifndef CAT_HPP_
#define CAT_HPP_

class Cat : public virtual WildAnimal {
public:
	Cat();
	Cat(std::string nameCat);

	virtual void giveVoice();
	virtual ~Cat();
};

#endif /* CAT_HPP_ */
