
#include <string>

#include "Cat.hpp"
#include "Dog.hpp"
#include "WildAnimal.hpp"

#ifndef CATDOG_HPP_
#define CATDOG_HPP_

class CatDog : public Cat, public Dog {
public:
	CatDog(std::string nameCatDog);

	void giveVoice();

	virtual ~CatDog();
};

#endif /* CATDOG_HPP_ */
