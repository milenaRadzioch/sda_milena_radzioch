
#include "WildAnimal.hpp"
#ifndef DOG_HPP_
#define DOG_HPP_

class Dog :  public virtual WildAnimal {
public:
	Dog();
	Dog(std::string nameDog);

	virtual void giveVoice();
	virtual ~Dog();
};

#endif /* DOG_HPP_ */
