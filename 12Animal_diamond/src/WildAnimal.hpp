
#include <string>


#ifndef WILDANIMAL_HPP_
#define WILDANIMAL_HPP_

class WildAnimal {

public:
	std::string mNameAnimal;
public:
	WildAnimal(std::string nameAnimal);
	virtual ~WildAnimal();

	virtual void giveVoice();
};

#endif /* WildAnimal_HPP_ */
