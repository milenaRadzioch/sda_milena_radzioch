#include <iostream>

#include "Cat.hpp"
#include "CatDog.hpp"
#include "Dog.hpp"
#include "WildAnimal.hpp"


int main() {
	WildAnimal* tab[6];

	tab[0] = new Cat("Filemon");
	tab[1] = new Dog("Szarik");
	tab[2] = new Cat("Fiona");
	tab[3] = new Dog("Toffik");
	tab[4] = new CatDog("Dogocat");
	tab[5] = new CatDog("CatoDog");


	for(int i =0; i<6; i++)
	{
		tab[i]->giveVoice();
	}

	for(int i =0; i<6; i++)
	{
		delete tab[i];

	}

	return 0;
}
