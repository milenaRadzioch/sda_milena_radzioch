#include <iostream>

#include "Cube.hpp"

int main()
{
	Cube k10(10);
	Cube k6(6);
	Cube k20(20);

	std::cout<<"K10="<<k10.getValue()<<" K6="<<k6.getValue()<<" K20="<<k20.getValue()<<"\n";

	return 0;
}
