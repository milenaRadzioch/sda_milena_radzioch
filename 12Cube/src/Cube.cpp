#include "Cube.hpp"


Cube::Cube(unsigned int numberWalls)
: mValue()
, mNumberWalls(numberWalls)
{
	std::srand(std::time(0));
	this->random();
}

void Cube::random()
{
	mValue = std::rand() % mNumberWalls + 1;
}

unsigned int Cube::getValue()
{

	return mValue;
}
