/*
 * List.hpp
 *
 *  Created on: 23.04.2017
 *      Author: MP
 */

#ifndef LIST_HPP_
#define LIST_HPP_

#include <iostream>

class List
{
private:
	class Wezel
	{
	public:
		int mWartosc;
		Wezel * nast;

		Wezel(int wartosc)
		{
			mWartosc = wartosc;
			nast = 0;
		}
	};

	Wezel* head; //wksaznik na pierwszy element
	Wezel* tail; //wskaznik na ostatni element

public:
	List()
	{
		head = 0;
		tail = 0;
	}

	void wypisz()
	{
		Wezel* tmp = 0; //tmo b�dzie wskaznikiem na aktualnie przegladany wezel
		tmp = head; //ustaw aktualny przegladany wezel na poczatek listy

		std::cout << "{ ";
		while (tmp != 0) //test czy nie jest to ostatni element
		{
			std::cout << "[" << tmp->mWartosc << "] ";
			tmp = tmp->nast; //pobierz nastepny element z listy
		}
		std::cout << " }" << std::endl;
	}

	void dodajPoczatek(int wartosc)
	{
		if (head != 0) //sprawdz czy nasza lista jest pusta
		{ //jak nie jest pusta
			Wezel* nowyWezel = new Wezel(wartosc); //stw�rz nowy w�zel a nast�pnie przypisz go do tymczasowego wskaznika
			nowyWezel->nast = head; //Niech nowy wezel wskazuje na to na co wskazuje czo�o listy
			head = nowyWezel; //Niech czo�o listy wskazuje teraz na nasz nowy Wezel
		}
		else
		{ //jak jest pusta
			head = new Wezel(wartosc); //stw�rz nowy Wezel, nastepnie niech czo�o na niego wskazuje
			tail = head; //ustaw ogon na nowy element (czyli na ten ktory wskazuje head)_
		}
	}

	void dodajKoniec(int wartosc)
	{
		if (tail != 0)
		{
			Wezel* nowyWezel = new Wezel(wartosc);
			tail->nast = nowyWezel;
			//nowyWezel->nast = tail;
			tail = nowyWezel;
		}
		else
		{
			tail = new Wezel(wartosc);
			head = tail;
		}

	}

	int pobierz(int indeks) //funkcja zwracajaca wartosc elemetnu pod pozycja n
	{
		Wezel* tmp = head;
		int licznikOdwiedzin = 0;

		while (tmp != 0)
		{
			licznikOdwiedzin++;
			if (licznikOdwiedzin == indeks)
			{
				return tmp->mWartosc;
			}
			else
			{
				tmp = tmp->nast;
			}

		}

		return 0; //ma zwracac wartosc wezla
	}

	bool czyPusta() // funkcja zwracajaca true jesli lista jest pusta
	{
		if (head != 0)
		{
			return false;
		}
		else
		{
			return true;
		}

	}

	void wyczysc() //funkcja usuwajace wszystkie Wezly z listy
	{
		Wezel* tmp = 0;
			tmp = head;


		while (tmp != 0) //while (tmp != tail)
		{
			head = tmp->nast;
			delete tmp;
			tmp = head;
		}
		//delete tail;


		head = 0;
		tail = 0;
	}

	void usun(int indeks) //funkcja ususwajace element na podanej pozycji od head
	{

	}
//int znajdzPozycje (int wartosc){
//	int pozycja = 0;
//	rerutn pozycja;
//}

};


#endif /* LIST_HPP_ */
