#include "Date.hpp"
#include <iostream>

Date::Date() :
		mDay(1), mMonth(1), mYear(1900) {

}
Date::Date(unsigned int day, unsigned int month, unsigned int year) {
	setDay(day);
	setMonth(month);
	setYear(year);
};

Date::~Date() {
}

unsigned int Date::getDay() const {
	return mDay;
}

void Date::setDay(unsigned int day) {
	if (day < 1)
		mDay = 1;
	else if (day > 31)
		mDay = 31;
	else
		mDay = day;
}

unsigned int Date::getMonth() const {
	return mMonth;
}

void Date::setMonth(unsigned int month) {
	if (month < 1) {
		mMonth = 1;
	} else if (month > 12)
		mMonth = 12;
	else
		mMonth = month;
}

unsigned int Date::getYear() const {
	return mYear;
}

void Date::setYear(unsigned int year) {

	mYear = year;
}

void Date::write() {
	std::cout << "Day" << mDay << "Month" << mMonth << "Year" << mYear
			<< std::endl;
}
void Date::moveYear(int howYear) {
	mYear += howYear;
}
void Date::moveMonth(int howMonth) {
	mMonth += howMonth;
}
void Date::moveDay(int howDay) {
	mDay += howDay;
}

int Date::differenceBetweenDates(Date data) {
	long int NrDaysFirstDate = getYear() * 365 + getDay();
	NrDaysFirstDate += (getMonth() - 1) * 31;

	long int NrDaysSecDate = data.getYear() * 365 + data.getDay();
	NrDaysSecDate += (data.getMonth() - 1) * 31;

	int difference = NrDaysSecDate - NrDaysFirstDate;
	return difference;
}
