

#ifndef DATE_HPP_
#define DATE_HPP_

class Date {
protected:
	unsigned int mDay;
	unsigned int mMonth;
	unsigned int mYear;
public:
	Date();
	Date(unsigned int day, unsigned int month, unsigned int year);
	virtual ~Date();

	unsigned int getDay() const;
	void setDay(unsigned int day);
	unsigned int getMonth() const;
	void setMonth(unsigned int month);
	unsigned int getYear() const;
	void setYear(unsigned int year);

	void write();
	void moveYear(int howYear);
	void moveMonth(int howMonth);
	void moveDay(int howDay);
	int differenceBetweenDates(Date data);
};

#endif /* DATE_HPP_ */
