
#include "Person.hpp"
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

Person::Person() :
		Pesel(0), mName("name"), mSurname("surname"), mSex(Person::female) {

}

Person::Person(std::string name, std::string surname, int* pesel, Sex sex) {
	setName(name);
	setSurname(surname);
	setPesel(pesel);
	setSex(sex);
}

std::string Person::getName() {
	return mName;
}

void Person::setName(std::string name) {
	mName = name;
}

std::string Person::getSurname() {
	return mSurname;
}

void Person::setSurname(std::string surname) {
	mSurname = surname;
}

Person::Sex Person::getSex() {
	return mSex;
}

void Person::setSex(Sex sex) {
	mSex = sex;
}

std::string Person::getSexString() {

	switch (mSex) {
	case female:
		return "Female";
		break;
	case male:
		return "Male";
		break;
	default:
		return "";
		break;
	}

	return "";

}

void Person::write() {

	cout << "name: " << mName << ", surname: " << mSurname << ", pesel: ";

	for (int k = 0; k < 11; k++) {
		cout << getPesel()[k];
	}

	cout << ", date of birth: " << takeDate().getDay() << "-"
			<< takeDate().getMonth() << "-" << takeDate().getYear() << ", sex: "
			<< getSexString() << endl;
}

bool Person::orOlder(Person osoba) {
	int subtraction = takeDate().differenceBetweenDates(osoba.takeDate());
	if (subtraction > 0) {
		return true;
	} else {
		return false;
	}
}

Person::~Person() {

}

