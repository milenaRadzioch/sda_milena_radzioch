#include <string>
#include "Pesel.hpp"
#include "Date.hpp"

#ifndef OSOBA_HPP_
#define OSOBA_HPP_

class Person : public Pesel{

public:
	enum Sex {
		female, male
	};

	Person();

	Person(std::string name, std::string surname, int pesel[], Sex sex);
	virtual ~Person();

	std::string getName();
	void setName(std::string name);
	std::string getSurname();
	void setSurname(std::string surname);
	Sex getSex();
	void setSex(Sex sex);
	void write();
	bool orOlder(Person osoba);


	std::string getSexString();

private:
	std::string mName;
	std::string mSurname;
	Sex mSex;

};

#endif /* OSOBA_HPP_ */
