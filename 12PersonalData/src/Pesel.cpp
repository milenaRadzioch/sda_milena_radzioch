
#include "Pesel.hpp"
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

Pesel::Pesel() :
		mPesel(0) {

}

Pesel::Pesel(int *pesel) {
	setPesel(pesel);
}

int* Pesel::getPesel() {
	return mPesel;
}

int replaceDateToInt(std::string prefix, int value1, int value2) {
	int replaceInt;
	ostringstream oss;
	oss << prefix << value1 << value2;
	istringstream iss(oss.str());
	iss >> replaceInt;
	return replaceInt;
}

void Pesel::setPesel(int pesel[]) {

	int sum = pesel[0] * 1 + pesel[1] * 3 + pesel[2] * 7 + pesel[3] * 9
			+ pesel[4] * 1 + pesel[5] * 3 + pesel[6] * 7 + pesel[7] * 9
			+ pesel[8] * 1 + pesel[9] * 3;

	if (((10 - sum % 10) % 10) == pesel[10]) {

		mPesel = pesel;

		mBirthDate = Date(replaceDateToInt("", pesel[4], pesel[5]),
				replaceDateToInt("", pesel[2], pesel[3]),
				replaceDateToInt("19", pesel[0], pesel[1]));
	} else {
		cout << "Pesel incorrect" << endl;

		for (int k = 0; k < 11; k++) {
			pesel[k] = 0;
		}
	}
}
Date Pesel::takeDate() {
	return mBirthDate;
}

std::string Pesel::stringPesel() {
	ostringstream oss;
	oss << mPesel[0] << mPesel[1] << mPesel[2] << mPesel[3] << mPesel[4]
			<< mPesel[5] << mPesel[6] << mPesel[7] << mPesel[8] << mPesel[9]
			<< mPesel[10];
	return oss.str();
}

Pesel::~Pesel() {

}

