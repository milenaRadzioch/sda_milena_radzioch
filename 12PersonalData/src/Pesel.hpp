
#include <string>
#include "Date.hpp"

#ifndef PESEL_HPP_
#define PESEL_HPP_

class Pesel {

private:
	int* mPesel;
	Date mBirthDate;

public:
	Pesel();
	Pesel(int pesel[]);

	int* getPesel();
	void setPesel(int* pesel);
	Date takeDate();
	std::string stringPesel();

	virtual ~Pesel();
};

#endif /* PESEL_HPP_ */
