#include <iostream>

#include "Person.hpp"
using namespace std;

int main() {

	int pesel1[] = { 8, 7, 0, 4, 2, 4, 0, 8, 9, 4, 8 };

	Person person1("Milena", "Friday", pesel1, Person::female);
	person1.write();

	int pesel2[] = { 9, 9, 0, 7, 2, 9, 0, 4, 9, 9, 0 };

	Person person2("John", "Snow", pesel2, Person::male);
	person2.write();

	cout << person1.orOlder(person2);

	return 0;
}
