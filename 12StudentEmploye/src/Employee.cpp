#include "Employee.hpp"

#include <iostream>
#include <string>

void Employee::work() {
	std::cout << " I work hard! " << std::endl;
}

Employee::Employee(float salary, std::string profession, long long nrPESEL) :
		Person(nrPESEL), mSalary(salary), mProfession(profession) {
}

Employee::Employee(float salary, std::string profession) :
		mSalary(salary), mProfession(profession)

{
}
Employee::~Employee() {

}
