#include <string>
#include "Person.hpp"

#ifndef EMPLOYEE_HPP_
#define EMPLOYEE_HPP_

class Employee: public virtual Person {
private:
	float mSalary;
	std::string mProfession;

public:
	virtual void work();

	Employee(float salary, std::string profession, long long nrPESEL);
	Employee(float salary, std::string profession);
	virtual ~Employee();
};

#endif /* EMPLOYEE_HPP_ */
