#include "Student.hpp"
#include "Person.hpp"
#include <iostream>

int Student::EnterNr() {
	return mNrIndex;
}

void Student::learn() {

	std::cout << "Student::Learn!" << std::endl;

}
Student::Student(int nrIndex, std::string specialty, long long nrPESEL) :
		Person(nrPESEL), mNrIndex(nrIndex), mSpecialty(specialty) {

}
Student::Student(int nrIndex, std::string specialty) :
		mNrIndex(nrIndex), mSpecialty(specialty) {

}
Student::~Student() {

}
