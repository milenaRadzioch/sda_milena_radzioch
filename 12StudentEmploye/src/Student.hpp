#include <string>
#include "Person.hpp"

#ifndef STUDENT_HPP_
#define STUDENT_HPP_

class Student: public virtual Person {
private:
	int mNrIndex;
	std::string mSpecialty;

public:
	int EnterNr();
	virtual void learn();

	Student(int nrIndex, std::string specialty, long long nrPESEL);
	Student(int nrIndex, std::string specialty);
	virtual ~ Student();

};

#endif /* STUDENT_HPP_ */
