#include "WorkingStudent.hpp"

WorkingStudent::WorkingStudent(int nrIndex, std::string specialty, float salary,
		std::string profession, long long nrPESEL) :
		Person(nrPESEL), Student(nrIndex, specialty), Employee(salary,
				profession) {
	void work();
	void learn();
};
