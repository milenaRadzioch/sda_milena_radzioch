#include "Employee.hpp"
#include "Person.hpp"
#include "Student.hpp"

#ifndef WORKINGSTUDENT_HPP_
#define WORKINGSTUDENT_HPP_

void work();
void learn();

class WorkingStudent: public Student, public Employee {
public:
	WorkingStudent(int nrIndex, std::string specialty, float salary,
			std::string profession, long long nrPESEL);

};

#endif /* WORKINGSTUDENT_HPP_ */
