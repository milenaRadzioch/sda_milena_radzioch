#include "Employee.hpp"
#include "Student.hpp"
#include "WorkingStudent.hpp"

#include <iostream>
#include <string>

using namespace std;

int main() {

	WorkingStudent john(1234, "Biology", 1600, "Field of study", 87042465651);
	Employee employee(5678.0, "Mechanic", 97082335622);
	Student student(4201, "Field of study", 90012365635);

	std::cout << "Employee PESEL: " << employee.givePESEL() << std::endl;
	std::cout << "Student PESEL: " << student.givePESEL() << std::endl;
	std::cout << "Index number: " << john.EnterNr() << std::endl;
	std::cout << "Working student PESEL: " << john.givePESEL() << std::endl;

	return 0;
}
