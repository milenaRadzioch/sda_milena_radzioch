#include <iostream>
#include "SzyfrowanyTekst.hpp"

using namespace std;
#ifndef CEZARTEKST_HPP_
#define CEZARTEKST_HPP_

class CezarTekst : public SzyfrowanyTekst
{

private:
	string mTekst;

public:
	CezarTekst();

	virtual ~CezarTekst();

	void szyfruj();
	void wypisz();

};

#endif /* CEZARTEKST_HPP_ */
