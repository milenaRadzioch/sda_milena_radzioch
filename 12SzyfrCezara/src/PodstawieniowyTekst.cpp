#include <string>
#include "PodstawieniowyTekst.hpp"


PodstawieniowyTekst::PodstawieniowyTekst()
{
	cout << "Konstruktor PodstawowyTekst" << endl;
}

PodstawieniowyTekst::~PodstawieniowyTekst()
{
	cout << "Destruktor CezarTekst" << endl;
}

void PodstawieniowyTekst::szyfruj()
{

	std::string zmieniony;
	for (unsigned int i = 0; i < getTekst().length(); i++)
	{
		int poz = getTekst()[i] - 'a';
		zmieniony[i] = alfabet[poz];
	}
	std::cout << "Zaszyfrowany tekst" << zmieniony << std::endl;
	setTekst(zmieniony);

}
void PodstawieniowyTekst::wypisz()
{
	cout << "Podstawowy tekst: " << getTekst() << endl;
}
