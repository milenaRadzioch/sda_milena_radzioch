
using namespace std;
#include <iostream>

#include "SzyfrowanyTekst.hpp"

#ifndef PODSTAWIENIOWYTEKST_HPP_
#define PODSTAWIENIOWYTEKST_HPP_

class PodstawieniowyTekst : public SzyfrowanyTekst
{

private:

	string alfabet = "QWERTYUIOPLKJHGFDSAZXCVBNM";

public:
	PodstawieniowyTekst();
	virtual ~PodstawieniowyTekst();

	void szyfruj();
	void wypisz();

};

#endif /* PODSTAWIENIOWYTEKST_HPP_ */
