#include "Wypisywalny.hpp"
#ifndef SZYFROWANYTEKST_HPP_
#define SZYFROWANYTEKST_HPP_

class SzyfrowanyTekst : public Wypisywalny {

private:
	bool jestZaszyfrowany;

public:
	SzyfrowanyTekst();
	virtual void szyfruj();
	virtual void wypisz();

	virtual ~SzyfrowanyTekst();
};

#endif /* SZYFROWANYTEKST_HPP_ */
