
#include "Wypisywalny.hpp"
using namespace std;

#include <iostream>


Wypisywalny::Wypisywalny()
:mTekst("tekst")
{
	cout << "Tworz�: %s " << mTekst << endl;
}

Wypisywalny::Wypisywalny(string tekst)
:mTekst(tekst)
{
	cout << "Tworz�: %s " << mTekst << endl;
}

void Wypisywalny::setTekst(string tekst) {
	mTekst = tekst;
}
string Wypisywalny::getTekst() {
	return mTekst;
}
void Wypisywalny::wypisz() {
	cout << "Tekst: " << mTekst << endl;
}

Wypisywalny::~Wypisywalny() {
	cout << "Niszcz�: %s " << mTekst << endl;
}

