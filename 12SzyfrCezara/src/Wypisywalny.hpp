
#include <string>

using namespace std;
#ifndef WYPISYWALNY_HPP_
#define WYPISYWALNY_HPP_

class Wypisywalny {

private:
	string mTekst;

public:
	Wypisywalny();
	Wypisywalny(string tekst);

	void setTekst(string tekst);
	string getTekst();
	void wypisz();
	virtual ~Wypisywalny();
};

#endif /* WYPISYWALNY_HPP_ */
