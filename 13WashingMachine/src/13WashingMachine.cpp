#include <iostream>
using namespace std;

class WashOption
{
private:
	int washSelection = 1;
public:
	int getwashSelection();
};
class Machine
{
public:

	virtual ~ Machine();
	virtual void turnOn() = 0;
	virtual void turnOff() = 0;


};

class Timer
{
private:
	int value = 1;
	int duration = 1;
public:
	void setDuration(int in);
	void start();
	int count();
	int getValue();
	int getDuration();
};

class Sensor
{
public:
	virtual bool check() = 0;
	virtual ~ Sensor();
};

class DoorSensor: public Sensor
{
public:
	bool check();

};

class TempSensor: public Sensor
{
public:
	bool check();

};
class Engine: public Machine
{
private:
	int rotation = 1;
public:
	void turnOn()
		{

		}
		void turnOff()
		{

		}
};


class WashingMachine: public Machine
{
private:
	int washTime = 1;
	int rinseTime = 1;
	int spinTime = 1;
	void wash();
	void rinse();

	void fill();
	void empty();
	void standardWash();
	void twiceRinse();
public:
	void main()
	{
		WashOption WashOption;
		switch (WashOption.getwashSelection())
		{
		case 1:
			standardWash();
			break;
		case 2:
			twiceRinse();
			break;
		case 3:
			spin();
			break;
		default:
			break;
		}
	}
	void turnOn()
	{

	}
	void turnOff()
	{

	}
	void spin()
	{
		Engine engine;

		engine.turnOn();
		Timer timer;
		timer.setDuration(60 * 60);

		timer.start();
		int time = timer.getValue();
		int duration = getDuration();

		while (time != duration)
		{
			time = timer.count();
		}
		engine.turnOff();
	}

};

class WaterSensor: public Sensor
{
public:
	int currentLevel = 1;
	int desiredLevel = 1;
	bool check();
};

int main()
{
	WashingMachine wm;
	wm.main();
	return 0;
}
