/*
 * WashingMachine.hpp
 *
 *  Created on: 23.04.2017
 *      Author: MP
 */

#ifndef WASHINGMACHINE_HPP_
#define WASHINGMACHINE_HPP_

class WashingMachine {
private:
	int washTime = 1;
	int rinseTime = 1;
	int spinTime = 1;

public:
	int main();

private:

	void wash();
	void rinse();
	void spin();
	void fill();
	void empty();
	void standardWash();
	void twiceRinse();
};

class Machine {
public:
	virtual void turnOn() = 0;
	virtual void turnOff() = 0;
	virtual ~ Machine();

};

class Time {
private:
	int value = 1;
	int duration = 1;
public:
	void setDuration(int in);
	void start();
	int count();
	int getValue();
	int getDuration();
};

class WashOption {
private:
	int washSelection = 1;
public:
	int getwashSelection();
};

class Engine {
private:
	int rotation = 1;
};

class Sensor {
public:
	virtual bool check() = 0;
	virtual ~ Sensor();
};

class DoorSensor: public Sensor {
public:
	bool check();

};

class TempSensor: public Sensor {
public:
	bool check();

};

class WaterSensor: public Sensor {
public:
	int currentLevel = 1;
	int desiredLevel = 1;
	bool check();
};

#endif /* WASHINGMACHINE_HPP_ */
