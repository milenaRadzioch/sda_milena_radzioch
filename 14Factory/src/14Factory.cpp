#include <iostream>
#include <string>
using namespace std;

class Processor
{
public:
	Processor()
	{

	}
	virtual ~Processor()
	{

	}
	virtual void process() = 0;
};
class ProcessorAMD: public Processor
{
public:
	ProcessorAMD()
	{

	}
	~ProcessorAMD()
	{

	}
	void process()
	{
		cout << "AMD" << endl;
	}
};
class ProcesorIntel: public Processor
{
public:
	ProcesorIntel()
	{

	}
	~ProcesorIntel()
	{

	}
	void process()
	{
		cout << "Intel" << endl;
	}
};

class Cooler
{
public:
	Cooler()
	{

	}
	virtual ~Cooler()
	{
		cout << "~cooler" << endl;
	}
	virtual void cool() = 0;
};
class CoolerAMD: public Cooler
{
public:
	CoolerAMD()
	{

	}
	~CoolerAMD()
	{

	}
	void cool()
	{
		cout << "AMD cool" << endl;
	}

};
class CoolerIntel: public Cooler
{
public:
	CoolerIntel()
	{

	}
	~CoolerIntel()
	{

	}
	void cool()
	{
		cout << "Intel cool" << endl;
	}
};

class AbstractSemiconductionFactory
{
public:
	virtual Processor* createProcessor() = 0;
	virtual Cooler* createCooler() = 0;

	AbstractSemiconductionFactory()
	{

	}
	virtual ~AbstractSemiconductionFactory()
	{

	}

};

class InterlSemiconductionFactory: public AbstractSemiconductionFactory
{
public:
	InterlSemiconductionFactory()
	{

	}
	~InterlSemiconductionFactory()
	{

	}
	Processor* createProcessor()
	{
		return new ProcesorIntel();
	}
	Cooler* createCooler()
	{
		return new CoolerIntel();
	}

};

class AMDSemiconducionFactory: public AbstractSemiconductionFactory
{
public:
	AMDSemiconducionFactory()
	{

	}

	~AMDSemiconducionFactory()
	{

	}

	Processor* createProcessor()
	{
		return new ProcessorAMD();
	}

	Cooler* createCooler()
	{
		return new CoolerAMD();
	}

};

class Computer
{
private:
	string mName;
	Processor* mProcessor;
	Cooler* mCooler;

public:
	Computer(string name, AbstractSemiconductionFactory& factory) :
			mName(name)
	{
		mProcessor = factory.createProcessor();
		mCooler = factory.createCooler();
	}
	~Computer()
	{
		delete mProcessor;
		delete mCooler;
	}
	void run()
	{
		cout << "my name is: " << mName << endl;
		mProcessor->process();
		mCooler->cool();

	}

};
int main()
{
	InterlSemiconductionFactory intelFactory;
	AMDSemiconducionFactory amdFactory;

	Computer intelPC("PC1", intelFactory);
	Computer amdPC("PC2", amdFactory);

	return 0;
}
