//============================================================================
// Name        : 14SpendRestAutomat.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;


//class SpendRestAutomat
//{
//public:
//	SpendRestAutomat(float reszta, float nominaly, float c)
//	:  mA(reszta)
//	,  mB(nominaly)
//	,  mC(c)
//{
//
//}
//
//	~SpendRestAutomat()
//	{
//	}
//private:
//	float mA;
//	float mB;
//	float mC;
//};

int main()
{
	int iloscNominal = 15;
	double nominaly [iloscNominal] = {500.0, 200.0, 100.0, 50.0, 20.0, 10.0, 5.0, 2.0, 1.0, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01};

	double reszta;
	cout << "Podaj reszte do wyplacenia: ";
	cin >> reszta;

	int i = 0;
	while (reszta > 0 && i < iloscNominal)
	{
		if (reszta >= nominaly[i])
		{
			int ileresztyWydac = reszta / nominaly[i];
			reszta = reszta - (nominaly[i] * ileresztyWydac);
			cout << nominaly[i] << "x" << ileresztyWydac << endl;

			}
		i++;
	}

	return 0;
}
