#include "Logger.hpp"

Logger::Logger(std::string filename, int log_level = 1)
: file(filename.c_str(), std::ofstream::out), level(log_level)
{

}

Logger::~Logger()
{
file.close();
}

void Logger::Log(std::string message, int log_level =3)
{
	if(level < log_level) return;

	time_t t;
	t = time(NULL);

	struct tm* current = localtime(&t);

	file << current->tm_year + 1900 << " " << current->tm_mon +1 << " " << current->tm_mday << " " << message << std::endl;

	//file << "[" << t << "]" << message << std::endl;


}
void Logger::Log(int a)
{
	time_t t;
	t = time(NULL);

	struct tm* current = localtime(&t);

	file << current->tm_year + 1900 << " " << current->tm_mon +1 << " " << current->tm_mday << " " << a << std::endl;

	//file << "[" << t << "]" << message << std::endl;


}
