/*
 * Logger.hpp
 *
 *  Created on: 23.04.2017
 *      Author: MP
 */

#include <iostream>
#include <fstream>
#include <string>

#include <ctime>

#ifndef LOGGER_HPP_
#define LOGGER_HPP_

class Logger
{
private:
	std::ofstream file;
	int level;

public:
	Logger(std::string filename, int log_level = 1);
	virtual ~Logger();
	void Log(std::string message, int log_level);

	void Log(int a);
};

#endif /* LOGGER_HPP_ */
