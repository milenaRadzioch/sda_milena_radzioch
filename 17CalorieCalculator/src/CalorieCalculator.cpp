/*
 * CalorieCalculator.cpp
 *
 *  Created on: 23.04.2017
 *      Author: MP
 */

#include "CalorieCalculator.hpp"


CalorieCalculator::~CalorieCalculator()
{

}

double CalorieCalculator::getWeight() const
{
	return mWeight;
}

void CalorieCalculator::setWeight(double weight)
{
	mWeight = weight;
}

int CalorieCalculator::getAge() const
{
	return mAge;
}

void CalorieCalculator::setAge(int age)
{
	mAge = age;
}

double CalorieCalculator::getGrowth() const
{
	return mGrowth;
}

void CalorieCalculator::setGrowth(double growth)
{
	mGrowth = growth;
}

CalorieCalculator::Sex CalorieCalculator::getSex() const
{
	return mSex;
}

void CalorieCalculator::setSex(Sex sex)
{
	mSex = sex;
}
float CalorieCalculator::calculateBMI()
{
	float BMI = getWeight() / (getGrowth() * getGrowth());
	return BMI;

}

float CalorieCalculator::calculateCalorie()
{
	switch (mSex)
	{
	case male:
		float calorieM;
		calorieM = 66.47 + (13.7 * mWeight) + (5.0 * mGrowth) - (6.76 * mAge);
		std::cout << calorieM << std::endl;
		break;
	case female:
		float calorieF;
		calorieF = 655.1 + (9.563 * mWeight) + (1.85 * mGrowth) - (4.676 * mAge);
		std::cout << calorieF << std::endl;
		break;
	case unknown:
		float calorieU;
		calorieU = 655.1 + (9.563 * mWeight) + (1.85 * mGrowth) - (4.676 * mAge);
		std::cout << calorieU << std::endl;
		break;
	}
	return 0;
}
