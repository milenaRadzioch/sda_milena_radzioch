#ifndef KALKULATORKALORII_HPP_
#define KALKULATORKALORII_HPP_
#include <string>
#include <iostream>


class CalorieCalculator
{

public:
	enum Sex
	{
		unknown, male, female
	};
private:
	Sex mSex;
	double mWeight;
	double mGrowth;
	int mAge;

public:
	CalorieCalculator()
	: mSex(unknown)
	, mWeight(0.0)
	, mGrowth(0.0)
	, mAge(0)
{

}
	CalorieCalculator(Sex sex, double weight, double growth, int age)
	:mSex(sex)
	, mWeight(weight)
	, mGrowth(growth)
	, mAge(age)
	{

	}

	virtual ~CalorieCalculator();
	double getWeight() const;
	void setWeight(double weight);
	int getAge() const;
	void setAge(int age);
	double getGrowth() const;
	void setGrowth(double growth);
	Sex getSex() const;
	void setSex(Sex sex);

	float calculateBMI();
	float calculateCalorie();

};



#endif /* CALORIECALCULATOR_HPP_ */
