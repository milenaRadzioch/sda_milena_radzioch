//============================================================================
// Name        : 17ConverterAscii.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//required google test
//framework https://github.com/google/googletest/tree/master/googletest

#include "gtest/gtest.h"
#include "ConverterAscii.hpp"
#include <string>

TEST(CalcAscii, OneTest)
{
	Calculator c("abc mnbhyg");
	EXPECT_EQ(971, c.asciiSum());
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
