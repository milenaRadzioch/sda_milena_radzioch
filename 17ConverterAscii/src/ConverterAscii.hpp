/*
 * ConverterAscii.hpp
 *
 *  Created on: 23.04.2017
 *      Author: MP
 */

#ifndef CONVERTERASCII_HPP_
#define CONVERTERASCII_HPP_

class Converter {
private:

	std::string mText;
public:
	Converter(std::string text) :
			mText(text) {
	}
	int asciiSum() {
		int mSum = 0;
//		cout << "Podaj tekst " << endl;
//		cin >> mTekst;

		for (unsigned int i = 0; i < mText.size(); i++) {
			mSum += mText[i];
		}
//		cout << "Ascii suma " << mSum << endl;
		return mSum;
	}

};

#endif /* CONVERTERASCII_HPP_ */
