/*
 * Cube.cpp
 *
 *  Created on: 23.04.2017
 *      Author: MP
 */

#include "Cube.hpp"
#include <cstdlib>
#include <ctime>

Cube::Cube(unsigned int numberWalls)
: mValue()
, mNumberWalls(numberWalls)
{
	std::srand(std::time(0));
	this->random();
}

void Cube::random()
{
	mValue = std::rand() % mNumberWalls + 1;
}

unsigned int Cube::getValue()
{

	return mValue;
}
