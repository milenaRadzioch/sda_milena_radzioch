/*
 * Cube.hpp
 *
 *  Created on: 23.04.2017
 *      Author: MP
 */

#ifndef CUBE_HPP_
#define CUBE_HPP_

class Cube {

public:
	Cube(unsigned int numberWalls);

	void random();
	unsigned int getValue();

private:
	unsigned int mValue;
	unsigned int mNumberWalls;
};

#endif /* CUBE_HPP_ */
