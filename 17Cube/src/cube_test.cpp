/*
 * cube_test.cpp
 *
 *  Created on: 23.04.2017
 *      Author: MP
 */

#include "gtest/gtest.h"
#include "Cube.hpp"

TEST(Cube, OneTest)
{
	const int walls = 10;
	Cube c(walls);
	c.random();
	EXPECT_GE(walls, c.getValue());
	c.random();
	EXPECT_LT(0, c.getValue());
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


