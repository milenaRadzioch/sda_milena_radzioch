
// http://osherove.com/tdd-kata-1/

#include "gtest/gtest.h"
#include "StringCalculator.hpp"

TEST(StringCalculator, EmptyString)
{
	StringCalculator c;
	EXPECT_EQ(0, c.Add(""));
}

TEST(StringCalculator, OneNumber)
{
	StringCalculator c;
	EXPECT_EQ(1, c.Add("1"));
	EXPECT_EQ(121, c.Add("121"));
}

TEST(StringCalculator, TwoNumber)
{
	StringCalculator c;
	EXPECT_EQ(3, c.Add("1,2"));
	EXPECT_EQ(33, c.Add("12,21"));
	EXPECT_EQ(153, c.Add("141,12"));
}


int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);
      return RUN_ALL_TESTS();
}
