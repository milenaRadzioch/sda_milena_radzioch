
#ifndef PASSWORD_HPP_
#define PASSWORD_HPP_

#include <string>
#include <cstdlib>

class Password{
public:
	int mLength;

	Password(int len) : mLength(len) {}



	char rollDigit()
	{
		return genChar('0',10);
	}

	char rollUpper()
	{
		return genChar ('A',26);
	}

	char rollLower()
	{
		return genChar ('a',26);
	}

	std::string genPasswd()
	{
		std::string pwd;

		pwd+= rollDigit();
		pwd+= rollUpper();
		pwd+= rollLower();

		for (int i=3; i<mLength ; i++)
		{
			int x = rand() % 3;
			if (x == 0)
			{
				pwd+= rollDigit();
			}
			else if (x==1)
			{
				pwd+=rollUpper();
			}
			else
			{
				pwd+=rollLower();
			}
		}
		mixString(pwd);

		return pwd;
	}

	void mixString(std::string& str)
	{
	    for(int i = str.length(); i > 0; i--)
	    {
	        int pos = rand()%str.length();
	        char tmp = str[i-1];
	        str[i-1] = str[pos];
	        str[pos] = tmp;
	    }
	}
private:
	char genChar(char begin, int range)
		{
			return begin + rand()%range;
		}
};

#endif /* PASSWORD_HPP_ */
