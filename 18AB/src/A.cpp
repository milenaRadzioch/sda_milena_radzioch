#include "A.hpp"
#include "B.hpp"

#include <iostream>

using namespace std;

A::A(int val)
:_val(val)
, _b(0)
{
}

void A::SetB(B *b)
{
    _b = b;
    cout<<"Inside SetB()"<<endl;
    _b->Print();
}

void A::Print()
{
    cout<<"Type:A val="<<_val<<endl;
}
A::~A()
{
	cout<<"~~A"<<endl;
}
