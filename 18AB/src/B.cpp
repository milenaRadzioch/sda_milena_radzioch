#include "B.hpp"
#include "A.hpp"
#include <iostream>

using namespace std;

B::B(double val)
:_val(val)
, _a(0)
{
}

void B::SetA(A *a)
{
    _a = a;
    cout<<"Inside SetA()"<<endl;
    _a->Print();
}

void B::Print()
{
    cout<<"Type:B val="<<_val<<endl;
}
B::~B()
{
	cout<<"~~B"<<endl;
}
