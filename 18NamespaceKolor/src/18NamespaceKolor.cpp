#include "Kwadrat.hpp"
#include "Kolor.hpp"
#include <iostream>
#include "Figura.hpp"
#include "Kolo.hpp"
using namespace std;
//using namespace Kolor;

int main()
{
	cout << Kolor::convertToString(Kolor::niebieski) << endl;
	cout << Kolor::convertToString(Kolor::zielony) << endl;
	//cout << convertToString(niebieski) << endl;


	Figura *wskJakasFigura = 0;

	Kwadrat jakisKwadrat;
	jakisKwadrat.setValue(12);
	jakisKwadrat.setKolorKw(Kolor::zielony);
//	jakisKwadrat.wypisz();
	wskJakasFigura = &jakisKwadrat;
	wskJakasFigura->wypisz();

	Kwadrat jakisKwadrat2;
	jakisKwadrat2.setValue(33);
	jakisKwadrat2.setKolorKw(Kolor::czerwony);
	wskJakasFigura = &jakisKwadrat2;
	wskJakasFigura->wypisz();

	Kolo jakiesKolo;
	jakiesKolo.setR(33);
	jakiesKolo.setKolorKolo(Kolor::czarny);
	wskJakasFigura = &jakiesKolo;
	wskJakasFigura->wypisz();

	return 0;
}
