/*
 * Kolo.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Kolo.hpp"
#include <iostream>

Kolo::Kolo() :
		kolorKolo(Kolor::niebieski), mR(0)
{

}
void Kolo::wypisz()
{
	std::cout << "kolor ko�a:" << Kolor::convertToString(kolorKolo) << std::endl;
	std::cout << "pole ko�a:" << pole() << std::endl;
}
float Kolo::pole()
{
	return 3.14 * mR * mR;
}

Kolor::Kolor Kolo::getKolorKolo() const
{
	return kolorKolo;
}

void Kolo::setKolorKolo(Kolor::Kolor kolorKolo)
{
	this->kolorKolo = kolorKolo;
}

float Kolo::getR() const
{
	return mR;
}

void Kolo::setR(float r)
{
	mR = r;
}

Kolo::~Kolo()
{
}

