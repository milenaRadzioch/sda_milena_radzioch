#include "Kolor.hpp"
#include "Figura.hpp"

#ifndef KOLO_HPP_
#define KOLO_HPP_

class Kolo: public Figura
{
private:
	Kolor::Kolor kolorKolo;
	float mR;

public:
	Kolo();
	virtual ~Kolo();
	void wypisz();
	float pole();
	Kolor::Kolor getKolorKolo() const;
	void setKolorKolo(Kolor::Kolor kolorKolo);
	float getR() const;
	void setR(float r);
};

#endif /* KOLO_HPP_ */

