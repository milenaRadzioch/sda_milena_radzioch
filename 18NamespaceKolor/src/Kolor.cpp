#include "Kolor.hpp"
#include <string>

std::string Kolor::convertToString(Kolor kolor)
{
	std::string ret = "";
	switch (kolor)
	{
	case Kolor::czerwony:
		ret = "czerwony";
		break;
	case Kolor::zielony:
		ret = "zielony";
		break;
	case Kolor::niebieski:
		ret = "niebieski";
		break;
	default:
		return "nie ma takiego koloru";
	}
return ret;
}

