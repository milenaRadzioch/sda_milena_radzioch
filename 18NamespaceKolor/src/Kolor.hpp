
#ifndef KOLOR_HPP_
#define KOLOR_HPP_
#include <string>

namespace Kolor{

enum Kolor {
	czerwony,
	zielony,
	niebieski,
	czarny,
	szarny,
	fioletowy,
	srebrny

};

std::string convertToString(Kolor kolor);
}


#endif /* KOLOR_HPP_ */
