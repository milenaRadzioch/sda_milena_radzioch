/*
 * Kwadrat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Kwadrat.hpp"
#include <iostream>

Kwadrat::Kwadrat() :
		kolorKw(Kolor::zielony), value(0)
{
}
void Kwadrat::wypisz()
{
	std::cout << "kolor kwadratu:" << Kolor::convertToString(kolorKw) << std::endl;
	std::cout << "pole kwadratu:" << pole() << std::endl;
}
int Kwadrat::pole()
{
	return value * value;
}
Kolor::Kolor Kwadrat::getKolorKw() const
{
	return kolorKw;
}

void Kwadrat::setKolorKw(Kolor::Kolor kolorKw)
{
	this->kolorKw = kolorKw;
}

int Kwadrat::getValue() const
{
	return value;
}

void Kwadrat::setValue(int value)
{
	this->value = value;
}

Kwadrat::~Kwadrat()
{
}

