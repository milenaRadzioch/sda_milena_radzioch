/*
 * Kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_
#include "Kolor.hpp"
#include "Figura.hpp"

class Kwadrat: public Figura
{
private:
	Kolor::Kolor kolorKw;
	int value;

public:
	Kwadrat();
	virtual ~Kwadrat();
	void wypisz();
	int pole();
	Kolor::Kolor getKolorKw() const;
	void setKolorKw(Kolor::Kolor kolorKw);
	int getValue() const;
	void setValue(int value);
};

#endif /* KWADRAT_HPP_ */
