//============================================================================
// Name        : 18Replication.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <cstdlib>
#include <ctime>
#include "Unit.hpp"
#include "Group.hpp"
#include <iostream>

using namespace std;

int main()
{

	srand(time(NULL));

	Group armyOne;
	Unit* motherOne = new Unit("1");

	armyOne.add(motherOne);
	motherOne->addToGroup(&armyOne);

	Unit* motherTwo = new Unit("2");
	armyOne.add(motherOne);
	motherTwo->addToGroup(&armyOne);

	armyOne.printUnits();
	armyOne.replicateGroup();
	armyOne.printUnits();
	armyOne.replicateGroup();
	armyOne.printUnits();

	return 0;
}
