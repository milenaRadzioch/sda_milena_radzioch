/*
 * Group.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "Group.hpp"
#include "Unit.hpp"

Group::Group() :
		mUnits(0), mSize(0)
{
}

Group::~Group()
{
	clear();
}
void Group::resize()
{

	if (mSize == 0)
	{
		mSize += 1;
		mUnits = new Unit*[mSize];
	}
	else
	{
		mSize++;
		Unit** newUnitArray = new Unit*[mSize];

		for (unsigned int i = 0; i < mSize-1; i++)
		{
			newUnitArray[i] = mUnits[i];
		}
		delete[] mUnits;

		mUnits = newUnitArray;
	}

//powiekszyc tab i przepisac do nowej wiekszej tab wszystkich elementow
}
void Group::add(Unit* unit)
{
	resize();
	mUnits[mSize - 1] = unit;
//spr rozmiar tablicy, wola func zwieksz rozmia tab od size +1
	//a nast ustawia wskaznik na przekazana mu jednostke
}
void Group::clear()
{
	for (unsigned int i = 0; i < mSize; i++)
	{
		delete mUnits[i];
	}
	delete[] mUnits;
	mSize = 0;
	mUnits = 0;

//size na 0, usuwa wszsystkie jednostki, nastepnie tablice, rozmiar na 0 i wywolac destruktor,
	//w for delete do wszytkich elementow
}
void Group::replicateGroup()
{
	unsigned int currentSize = mSize;
	for (unsigned int i = 0; i < currentSize; i++)
	{
		mUnits[i]->replicate();
	}
}
void Group::printUnits()
{
	for (unsigned int i = 0; i < mSize; i++)
	{
		mUnits[i]->printId();
	}
}
