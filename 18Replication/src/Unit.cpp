/*
 * Unit.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Unit.hpp"
#include "Group.hpp"

Unit::Unit(std::string id) :
		mId(id), mGroupPtr(0)
{
}

Unit::~Unit()
{
}

void Unit::printId()
{
	std::cout << mId << std::endl;
}
void Unit::replicate()
{

	int idVariation = rand() % 10;
	char newId = '0' + idVariation;

	Unit* newborn = new Unit(mId + newId);
	newborn->addToGroup(mGroupPtr);
	//mGroupPtr->add(newborn);

}
void Unit::addToGroup(Group* group)
{
	mGroupPtr = group;
	mGroupPtr->add(this);

}
