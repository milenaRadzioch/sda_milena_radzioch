/*
 * Unit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include <iostream>
#include <string>

#ifndef UNIT_HPP_
#define UNIT_HPP_

class Group;

class Unit
{
private:
 std::string mId;
 Group* mGroupPtr;

public:
	Unit(std::string id);
	~Unit();
	void printId();
	void replicate();
	void addToGroup(Group* group);
};

#endif /* UNIT_HPP_ */
