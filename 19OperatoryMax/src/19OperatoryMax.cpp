#include <iostream>
using namespace std;

int main()
{

	int size = 10;
	int tab[size] =
	{ 97, 5, 12, 7, 44, 3, 55, 98, 11, 8 };
	int max = tab[0];

	for (int i = 1; i < size; i++)
	{
		max = (tab[i] > max) ? tab[i] : max;
	}
	cout << " MAX = " << max << endl;

	return 0;
}
