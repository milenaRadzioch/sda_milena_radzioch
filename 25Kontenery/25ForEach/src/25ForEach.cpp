#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

void printIntigerPart(float x) {
	cout << static_cast<int>(x) << " ";
}

void print(float x) {
	cout << x << " ";
}

void castIntigerPart(float& x) {
	x = static_cast<int>(x);
}

struct Sum   // funktor ktory przechowuje jakas wartosc i dodaje do niej
{
	Sum() :
			sum(0) {
	}
	void operator()(float x) {
		sum += x;
	}
	float sum;
};

int main() {

	list<float> num;

	num.push_back(33.123);
	num.push_back(33.13);
	num.push_back(21.1);
	num.push_back(0.3);
	num.push_back(1);
	num.push_back(23.123);
	num.push_back(98.75);

	for_each(num.begin(), num.end(), print);
	cout << endl;
	for_each(num.begin(), num.end(), castIntigerPart);
	for_each(num.begin(), num.end(), print);

	Sum suma = for_each(num.begin(), num.end(), Sum());

	cout << "suma = " << suma.sum;

	return 0;
}
