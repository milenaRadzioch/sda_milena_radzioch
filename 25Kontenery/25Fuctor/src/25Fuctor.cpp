#include <iostream>
using namespace std;

struct kukuctor {
	void operator()() const {
		cout << " KU KU" << endl;
	}
};

class add_x {
public:
	add_x(int x) :
			x(x) {
	}
	int operator()(int y) const {
		return y + x;
	}
private:
	int x;

};

int main() {
	add_x add42(42);
	int i = add42(999);
	cout << i << endl;

	cout << add_x(7)(8);

	kukuctor aaaa;
	kukuctor()();
	aaaa();

	return 0;
}
