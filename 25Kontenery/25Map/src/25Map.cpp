#include <iostream>
#include <utility>
#include <string>
#include <map>

using namespace std;

int main() {

	map<int, string> pracownicy;
	pracownicy.insert(pair<int, string>(1, "Maniek")); //klucze musza byc rozne, moga byc takie same imiona ale klucz czyli first musza byc rozne
	pracownicy.insert(pair<int, string>(6, "Stachu"));
	pracownicy.insert(pair<int, string>(3, "Grazynka"));

	for (map<int, string>::iterator it = pracownicy.begin();
			it != pracownicy.end(); ++it)
	{
		cout << it->second << endl;
		//cout << it->first << endl;
	}

	cout << "pracownik nr 3: " << pracownicy.find(3)->second << endl;
	cout << "pracownik nr 6: " << pracownicy.find(6)->second;
	return 0;
}
