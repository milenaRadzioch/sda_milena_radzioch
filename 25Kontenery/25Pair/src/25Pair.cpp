#include <iostream>
#include <utility>
#include <string>

using namespace std;

int main() {
	pair<string, int> uczen1;
	pair<string, int> uczen2("Tomek", 2);

	uczen1 = make_pair("Arek", 5);

	cout << uczen1.first << " " << uczen2.second << endl;

	return 0;
}
