#include <iostream>
#include <set>
using namespace std;

int main() {
	set<string> imiona;

	string tabString[] = { "Kamil", "Rafal", "Karol" };
	imiona.insert(tabString, tabString + 3);

	imiona.insert("Ala");
	imiona.insert("Kasia");
	imiona.insert("Basia");
	imiona.insert("Zosia");
	imiona.insert("Rafal");

	for (set<string>::iterator it = imiona.begin(); it != imiona.end(); ++it) {
		cout << *it << endl;
	}

	return 0;
}
