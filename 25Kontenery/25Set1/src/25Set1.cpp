#include <iostream>
#include <set>
using namespace std;

struct compareIntegralPart {
	bool operator()(float first, float second) const {
		return static_cast<int>(first) < static_cast<int>(second);
	}
};

int main() {

	set<float, compareIntegralPart> liczby;

	liczby.insert(2.1);
	liczby.insert(2.2);
	liczby.insert(3.5);
	liczby.insert(37.0);
	liczby.insert(4.8);
	liczby.insert(3.5);

	for (set<float>::iterator it = liczby.begin(); it != liczby.end(); ++it) {
		cout << *it << endl;
	}

	set<float>::iterator it;
	it = liczby.find(3.5);
	liczby.erase(it);
	cout << endl;

	for (set<float>::iterator it = liczby.begin(); it != liczby.end(); ++it) {
		cout << *it << endl;
	}

	return 0;
}
