#include <iostream>
#include <string>
#include <stack>
#include <vector>

using namespace std;

int main() {
	stack<char, vector<char> > stos; //stos

	string tekst = "abcdefg";
	//	cout << " Zrodlowy: " << tekst << endl;

	for (size_t i = 0; i < tekst.size(); i++) { //wrzucam w kontener
		stos.push(tekst[i]);
	}
	// cout << "Odwrocony";
	string reverse;
	while (!stos.empty()) {
		//cout << stos.top() << " ";
		reverse.push_back(stos.top());
		stos.pop();
	}

	cout << " Zrodlowy: " << tekst << " Odwrocony: " << reverse << endl;

	return 0;
}
