#include <iostream>
#include <vector>

using namespace std;

int main() {

	vector<int> vecInt(100, 8);
//	vector<string> vecString;
//
//	vecString.push_back("lala");
//	vecString.push_back("mjjhg");
//	vecString.push_back("na na na");
//	vecString.push_back("bla bla");
//	vecString.push_back("cos tam");

	cout << vecInt.capacity() << "; " << vecInt.size() << "; "
			<< vecInt.max_size() << endl;
	vecInt.reserve(1000);
	cout << vecInt.capacity() << "; " << vecInt.size() << "; "
			<< vecInt.max_size() << endl;
	vecInt.resize(1300, 12);
	cout << vecInt.capacity() << "; " << vecInt.size() << "; "
			<< vecInt.max_size() << endl;

	for (int i = 0; i < vecInt.size(); i++) {
		cout << vecInt[i] << " ";
	}

//	for (int i = 0; i < vecString.size(); i++) {
//		cout << vecString[i] << " ";
//	}
//	cout << endl;

	cout << "Usuwam vector: " << vecInt.size() << endl;

	while (!vecInt.empty()) {

		cout << vecInt.back() << endl;
		vecInt.pop_back();

	}

	return 0;
}
