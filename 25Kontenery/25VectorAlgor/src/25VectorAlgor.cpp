#include <string>
#include <iterator>
#include <iostream>
#include <vector>
#include <algorithm>
#include <deque>

using namespace std;

int main() {

	deque<int> maly;
	vector<int> duzy;

	for (int i = 1; i < 6; i++) {
		maly.push_back(i);
		duzy.push_back(i * 10);
	}

	std::front_insert_iterator< deque<int> > frontIt(maly); //pomocnicza zmienna frontIt
	copy(duzy.begin(), duzy.end(), frontIt); //do malego wrzucamy na poczatek duzy

//dodawanie na koniec malego konterena duzego
//	std::back_insert_iterator<deque<int> > backIt(maly);
//	copy(duzy.begin(), duzy.end(), backIt);

	//wypisywanie
	for (deque<int>::iterator it = maly.begin(); it != maly.end(); ++it)
	//lub for (deque<int>::iterator it = maly.begin(); it != maly.end(); advance(it,1))
			{
		cout << *it << " ";
	}
	cout << endl;
	//wypisywanie w inny sposob
	std::copy(maly.begin(), maly.end(),	std::ostream_iterator<int>(std::cout, " "));

	return 0;
}
