#include <iostream>
#include <list>

using namespace std;

bool sortowanieIntMalejace(const int& first, const int& second) {
	return first > second;
}
bool stringPoZnakach(const string& first, const string& second) {
	return first.size() < second.size();
}

int main() {

	list<int> listI;
	//	listI.push_back(7);
	//	listI.push_back(3);
	//	listI.push_front(77);
	//	listI.push_back(17);
	//	listI.push_back(3);
	//	listI.push_back(75);
	//	listI.push_back(11);
	//	listI.push_front(9);

	listI.push_back(7);
	listI.push_back(3);
	listI.push_back(0);
	listI.push_back(0);
	listI.push_back(3);
	listI.push_back(7);

// czy jest palindromem

	for (list<int>::iterator it = listI.begin(); it != listI.end(); ++it) {
		cout << *it << " ";
	}

	for(list<int>::reverse_iterator it = listI.rbegin(); it!= listI.rend(); ++it)
	{
		cout << *it << endl;
	}

//	listI.sort(sortowanieIntMalejace);
//	for (list<int>::iterator it = listI.begin(); it != listI.end(); ++it) {
//		cout << *it;
//	}

	bool palindrom = true;
	list<int>::iterator it = listI.begin();
	list<int>::reverse_iterator itr = listI.rbegin();

	while (it != listI.end() || itr != listI.rend())
	{

		if (*it != *itr)
		{
			palindrom = false;
			break;

		}
		++it;
		++itr;
	}
	cout << ((palindrom == true) ? " " : "nie ") << "jest palindromem" << endl;

//	list<string> sortowanie;
//
//	sortowanie.push_back("ala");
//	sortowanie.push_back("Cos");
//	sortowanie.push_front("Ktos");
//
//	for (list<string>::iterator it = sortowanie.begin(); it != sortowanie.end();
//			it++) {
//		cout << *it << endl;
//
//	}
//	cout << endl;
//	sortowanie.sort(stringPoZnakach);
//	for (list<string>::iterator it = sortowanie.begin(); it != sortowanie.end();
//			it++) {
//		cout << *it << endl;
//
//	}

//	lista<int>listI2;
//	listI2.push_back(11);

	return 0;
}
