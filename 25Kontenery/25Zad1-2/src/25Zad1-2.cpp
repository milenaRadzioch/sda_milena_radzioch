//1. Napisz funkcj�, kt�ra przyjmuje stringa, nast�pnie szereguje wszystkie
//	litery w porz�dku alfabetycznym i zwraca tak odwr�conego stringa (sort).
//2. Napisz analogiczn� funkcj�, kt�ra zwr�ci przemieszanego stringa (random_shuffle)

#include <string>
#include <list>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <ctime>

using namespace std;

string sortowanie(string word) {

	vector<char> str;
	cout << "Zrodlowy: " << word << endl;
	for (unsigned int i = 0; i < word.size(); i++) {
		str.push_back(word[i]);
	}
	std::sort(str.begin(), str.end());
	cout << "Uporzadkowany: ";

	string odwrocony;

	for (vector<char>::iterator it = str.begin(); it != str.end(); ++it) {
		odwrocony += *it;
	}
	cout << odwrocony;
	return odwrocony;
}

string shuffleString(string word) {
	std::random_shuffle(word.begin(), word.end());

	cout << word;
	return word;
}

int main() {
	srand(time(NULL));
	sortowanie("zabcgdefg");
	cout << endl;
	shuffleString("zabcgdefg");

	return 0;
}
