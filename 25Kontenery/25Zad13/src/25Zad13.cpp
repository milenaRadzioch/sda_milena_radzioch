// 13. Napisz funkcj�, kt�ra znajdzie pierwsz� wi�ksz� liczb�, kt�ra jest palindromem od zadanej liczby.

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <algorithm>
using namespace std;

bool czyPrawda(int x, int y)
{
	return (x==y);
}

vector<int> intVector(int x)
		{
	vector<int> vecInt;

	while(x !=0)
	{
		vecInt.push_back(x %10);
		x/=10;
	}

	return vecInt;
}

void najblizszyPalindrom(int x)
{
	vector<int> vecInt;
	do
	{
		vecInt = intVector(++x);
	}
while(!equal(vecInt.begin(), vecInt.end(), vecInt.rbegin()));

	cout<< x;
}

int main() {

	int a = 122;
	najblizszyPalindrom(a);


	return 0;
}
