//14. Wczytaj dane z pliku z list� kurs�w i ich dni/godzin
//    Format: [nazwa] [dzien] [poczatek] [koniec]
//    a)Wypisz podany plan uszeregowany wed�ug dnia tygodnia a nast�pnie godziny rozpocz�cia
//    b)Wypisz wszystkie kursy, kt�re maj� ze sob� konflikt godzin.

#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>
using namespace std;

struct Kurs {
	Kurs() : mNazwa(0), mDzien(0), mPoczatek(0), mKoniec(0) {
		}
	Kurs(string nazwa, int dzien, int poczatek, int koniec) :
			mNazwa(nazwa), mDzien(dzien), mPoczatek(poczatek), mKoniec(koniec) {
	}

	string mNazwa;
	int mDzien;
	int mPoczatek;
	int mKoniec;

	bool operator<(Kurs& drugi)
			{
		if(this->mDzien==drugi.mDzien)
		{
			return this->mPoczatek < drugi.mPoczatek;
			//godz
		}
		else
		{
			return this->mDzien < drugi.mDzien;
		}
			}
};

void wypisz(vector<Kurs>::iterator it, vector<Kurs>::iterator its)
{
	cout << "[" << it->mNazwa <<" ]"<<  " [" << it->mDzien << " ["<<it->mPoczatek <<" ]"<< " ["<< it->mKoniec << " ]"<< endl;
	cout  << "[" << its->mNazwa << " ]"<< " [" << its->mDzien << " ["<<its->mPoczatek << " ["<< its->mKoniec << " ]"<< endl;
}

int main() {

	vector<Kurs> kursy;

	fstream ifs;
	ifs.open("kursy.dat.txt", ios::in);

	if (!ifs.good())
	{
		cerr << "Error reading file" << endl;
		return 1;
	}
	string nazwa;
	int dzien;
	int poczatek;
	int koniec;

	while (!ifs.eof()) {

		ifs >> nazwa >> dzien >> poczatek >> koniec;
		kursy.push_back(Kurs(nazwa, dzien, poczatek, koniec));
	}


	sort(kursy.begin(), kursy.end());
	for (vector<Kurs>::iterator it = kursy.begin(); it != kursy.end(); ++it) {
		cout << " [ " <<it->mNazwa << " ] " << " [ " <<it->mDzien << " ] " << " [ " <<it->mPoczatek << " ] " << " [ " <<it->mKoniec << " ] "<< endl;
	}


	//do konfliktu
	int index = 1;
		for (vector<Kurs>::iterator it = kursy.begin(); it != kursy.end(); ++it) {
			for (vector<Kurs>::iterator its = kursy.begin() + index; its != kursy.end(); ++its) {

				if (it != its && it->mDzien == its->mDzien && it->mKoniec >= its->mPoczatek) {
					cout << "Znaleziono: ";
					wypisz(it, its);

				}
					}
			index++;
		}


	ifs.close();

	return 0;
}
