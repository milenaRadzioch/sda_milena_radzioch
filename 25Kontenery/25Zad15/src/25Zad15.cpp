// 15. Do poprzedniego zadania dodaj numer sali oraz drugi plik z list� student�w
//i ich kursami na ktore sie zapisali:
//    Format: [imie_Nazwisko] [liczba_kursow] [kurs1] [kurs2] .... [kursN]
//    a) Wyszukaj i wypisz wszystkie konfliktuj�ce kursy danego studenta
//    b) Wylicz ilu studentow uczeszcza na dany kurs (map)
//    c) Dodaj usuwanie zduplikowanych kursow

#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
using namespace std;

struct Kurs {
	Kurs() :
			mNazwa(""), mDzien(0), mPoczatek(0), mKoniec(0) {
	}
	Kurs(string nazwa, int dzien, int poczatek, int koniec) :
			mNazwa(nazwa), mDzien(dzien), mPoczatek(poczatek), mKoniec(koniec) {
	}

	string mNazwa;
	int mDzien;
	int mPoczatek;
	int mKoniec;

	bool operator<(Kurs& drugi) {
		if (this->mDzien == drugi.mDzien) {
			return this->mPoczatek < drugi.mPoczatek;

		} else {
			return this->mDzien < drugi.mDzien;
		}
	}
};

struct Student {
	Student() :
		mImieNazwisko(""), mLiczbaKursow(0), mNazwyKursow(0) {
	}
	Student(string imieNazwisko, int liczbaKursow, vector<string> nazwyKursow) :
		mImieNazwisko(imieNazwisko), mLiczbaKursow(liczbaKursow), mNazwyKursow(nazwyKursow) {
	}

	string mImieNazwisko;
	int mLiczbaKursow;
	vector<string> mNazwyKursow;
};

void wypisz(vector<Kurs>::iterator it, vector<Kurs>::iterator its) {
	cout << "[" << it->mNazwa << " ]" << " [" << it->mDzien << " [" << it->mPoczatek << " ]" << " [" << it->mKoniec << " ]" << endl;
	cout << "[" << its->mNazwa << " ]" << " [" << its->mDzien << " [" << its->mPoczatek << " [" << its->mKoniec << " ]" << endl;
}

vector<string> ciecieLiniPlikuPerSpacja(string liniaPliku) {
	vector<string> internal;
	stringstream ss(liniaPliku);
	string tok;

	while (getline(ss, tok, ' ')) {
		internal.push_back(tok);
	}

	return internal;
}

Kurs pobierKursPoNazwie(vector<Kurs> kursy, string nazwaKursu) {
	for (vector<Kurs>::iterator it = kursy.begin(); it != kursy.end(); ++it) {
		if (it->mNazwa == nazwaKursu) {
			return *it;
		}
	}
}

int main() {

	//Wczytywanie kursow
	vector<Kurs> kursy;
	fstream ifs;
	ifs.open("kursy.dat", ios::in);
	if (!ifs.good()) {
		cerr << "Error reading file" << endl;
		return 1;
	}
	string nazwa;
	int dzien;
	int poczatek;
	int koniec;
	int x;

	while (!ifs.eof()) {
		ifs >> nazwa >> dzien >> poczatek >> koniec >> x;
		kursy.push_back(Kurs(nazwa, dzien, poczatek, koniec));
	}

	sort(kursy.begin(), kursy.end());
	for (vector<Kurs>::iterator it = kursy.begin(); it != kursy.end(); ++it) {
		cout << " [ " << it->mNazwa << " ] " << " [ " << it->mDzien << " ] " << " [ " << it->mPoczatek << " ] " << " [ " << it->mKoniec	<< " ] " << endl;
	}

	ifs.close();

	//Wczytywanie studentow
	vector<Student> studenci;
	ifstream input("studenci.dat");
	string line;
	if (input) {
		{
			while (getline(input, line))
			{
				vector<string> params = ciecieLiniPlikuPerSpacja(line);
				string imieNazwisko;
				int liczbaKursow;
				vector<string> kursyStudenta;

				imieNazwisko = params.at(0);
				liczbaKursow = atoi(params.at(1).c_str());

				for (unsigned int i=2; i<params.size(); i++) {
					kursyStudenta.push_back(params.at(i));
				}

				studenci.push_back(Student(imieNazwisko, liczbaKursow, kursyStudenta));
			}
			input.close();
		}
	}

	//Wyswietlanie student�w
	for (vector<Student>::iterator it = studenci.begin(); it != studenci.end(); ++it) {
		cout << " [ " << it->mImieNazwisko << " ] " << " [ " << it->mLiczbaKursow << " ] " << endl;
		//Wyswietlanie kurs�w student�w
		for (vector<string>::iterator its = it->mNazwyKursow.begin(); its != it->mNazwyKursow.end(); ++its) {
				cout << " [ " << *its << endl;
		}

	}
	//a) Wyszukaj i wypisz wszystkie konfliktuj�ce kursy danego studenta

		for (vector<Student>::iterator it = studenci.begin(); it != studenci.end(); ++it) {
			int index = 1;
				for (vector<string>::iterator its = it->mNazwyKursow.begin(); its != it->mNazwyKursow.end(); ++its) {

					for (vector<string>::iterator itr = it->mNazwyKursow.begin() + index; itr != it->mNazwyKursow.end(); ++itr) {
						string nazwaPoprzednia = *its;
						string nazwaNastepna = *itr;

						Kurs poprzedni = pobierKursPoNazwie(kursy,nazwaPoprzednia);
						Kurs nastepny = pobierKursPoNazwie(kursy,nazwaNastepna);

						if (nazwaPoprzednia != nazwaNastepna && poprzedni.mDzien == nastepny.mDzien && poprzedni.mKoniec >= nastepny.mPoczatek) {

							cout << "Znaleziono: " << poprzedni.mDzien << " " << nastepny.mDzien << poprzedni.mKoniec << " " << nastepny.mPoczatek;
							cout<< "Student: " << it->mImieNazwisko << ", nazwa kursu: " << nazwaPoprzednia << "<->" << nazwaNastepna<<endl;
							//pokaz(it, its);
						}
					}
					index++;
				}

		}
	//b) Wylicz ilu studentow uczeszcza na dany kurs (map)
	map<string,int> kursyUczeszczanie;
	for (vector<Student>::iterator it = studenci.begin(); it != studenci.end(); ++it) {
		for (vector<string>::iterator its = it->mNazwyKursow.begin(); its != it->mNazwyKursow.end(); ++its) {
			map<string,int>::iterator it = kursyUczeszczanie.find(*its);
				if(it != kursyUczeszczanie.end())
				{
					kursyUczeszczanie.insert(std::pair<string,int>(*its,it->second++));
				} else {
					kursyUczeszczanie.insert(std::pair<string,int>(*its,1));
				}
		}
	}

	//Wypisanie
	for (map<string,int>::iterator it = kursyUczeszczanie.begin(); it != kursyUczeszczanie.end(); ++it) {
		cout<< "Kurs: " << it->first << ", ilo��: " << it->second << endl;
	}
	//c) Dodaj usuwanie zduplikowanych kursow


	for (vector<Student>::iterator it = studenci.begin(); it != studenci.end(); ++it) {
				int index = 1;
					for (vector<string>::iterator its = it->mNazwyKursow.begin(); its != it->mNazwyKursow.end(); ++its) {

						for (vector<string>::iterator itr = it->mNazwyKursow.begin() + index; itr != it->mNazwyKursow.end(); ++itr) {
							string nazwaPoprzednia = *its;
							string nazwaNastepna = *itr;

							Kurs poprzedni = pobierKursPoNazwie(kursy,nazwaPoprzednia);
							Kurs nastepny = pobierKursPoNazwie(kursy,nazwaNastepna);

							if (nazwaPoprzednia != nazwaNastepna && poprzedni.mDzien == nastepny.mDzien && poprzedni.mKoniec >= nastepny.mPoczatek) {

								cout << "Znaleziono: " << poprzedni.mDzien << " " << nastepny.mDzien << poprzedni.mKoniec << " " << nastepny.mPoczatek;
								cout<< "Student: " << it->mImieNazwisko << ", nazwa kursu: " << nazwaPoprzednia << "<->" << nazwaNastepna<<endl;
								//pokaz(it, its);
							}
						}
						index++;
					}

			}
	//do konfliktu
	int index = 1;
	for (vector<Kurs>::iterator it = kursy.begin(); it != kursy.end(); ++it) {
		for (vector<Kurs>::iterator its = kursy.begin() + index; its != kursy.end(); ++its) {

			if (it != its && it->mDzien == its->mDzien && it->mKoniec >= its->mPoczatek) {
				cout << "Znaleziono: ";
				wypisz(it, its);
			}
		}
		index++;
	}

	return 0;
}
