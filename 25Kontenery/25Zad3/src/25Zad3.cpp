// 3. Napisz funkcj�, kt�ra przyjmuje dwa stringi a nast�pnie zwraca vector ich wsp�lnych liter
//(powtarzajacych sie w obu stringach).
#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <algorithm>

using namespace std;

vector<char> WspolneLitery(string a, string b) {

	vector<char> str1(a.begin(), a.end());
	vector<char> str2(b.begin(), b.end());
	vector<char> duplikaty;

	for (vector<char>::iterator it1 = str1.begin(); it1 != str1.end(); ++it1) {
		for (vector<char>::iterator it2 = str2.begin(); it2 != str2.end();
				++it2) {
			if (*it1 == *it2) {
				duplikaty.push_back(*it2);
			}
		}
	}
	sort(duplikaty.begin(), duplikaty.end());
	vector<char>::iterator koniecUnique;
	koniecUnique = std::unique(duplikaty.begin(), duplikaty.end());
	duplikaty.resize(std::distance(duplikaty.begin(), koniecUnique));
	cout << string(duplikaty.begin(), duplikaty.end());

	return duplikaty;
}

int main() {
	string a = "sloswo";
	string b = "swsyrsaz";

	WspolneLitery(a, b);

	return 0;
}
