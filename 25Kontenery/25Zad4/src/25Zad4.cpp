// 4. Napisz funkcj�, kt�ra usunie spacje z podanego stringa. Zadanie wykonaj na dw�ch kontenrach:
//vector i list (remove i erase)

#include <string>
#include <list>
#include <vector>
#include <iostream>
using namespace std;

list<char> BezSpacji(string a) {
	list<char> znaki;

	for (unsigned int i = 0; i < a.size(); i++) {
		znaki.push_back(a[i]);
	}

	for (std::list<char>::iterator it = znaki.begin(); it != znaki.end();)
	{
	    if (*it == ' ') {
	    	it = znaki.erase(it);
	    }
	    else {
	    	++it;
	    }

	}
	//znaki.remove(' ');

	return znaki;
}

int main() {

	string tekst = "Jakies tekst ze spacjami";
	list<char> z = BezSpacji(tekst);

	for (list<char>::iterator it = z.begin(); it != z.end(); ++it) {
		cout << *it;
	}


	return 0;
}
