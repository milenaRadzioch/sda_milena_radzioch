//5. Napisz funkcj�, kt�ra zliczy wyst�pienia podanej litery w podanym stringu (count).

#include <map>
#include <string>
#include <iostream>
#include <utility>
using namespace std;

map<char, int> Zliczanie(string tekst) {

	map<char, int> zlicz;

	for (unsigned int i = 0; i < tekst.size(); i++) {
		int z = zlicz.find(tekst[i])->second;
		if (z > 0) {
			z++;
			zlicz[tekst[i]] = z;
		} else {
			zlicz.insert(pair<char, int>(tekst[i], 1));
		}
	}

	return zlicz;
}

int main() {

	string text = "sasasasaswwwaaaaaaaaaa";
	map<char, int> u = Zliczanie(text);
	for (map<char, int>::iterator it = u.begin(); it != u.end(); ++it)
		{
			cout << it->first << endl;
			cout << it->second << endl;
		}
	return 0;
}
