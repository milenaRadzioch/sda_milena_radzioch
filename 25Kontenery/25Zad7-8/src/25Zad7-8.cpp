//7. Napisz funkcj�, kt�ra stworzy wektor przechowuj�cy pierwiastki kwadratowe z podanego zakresu
//np od. 3 do 10 (for_each)
//
//8. Zmie� poprzednie zadanie tak aby zwraca�o sum� kwadrat�w (accumulate)

#include <map>
#include <list>
#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <math.h>
#include <algorithm>
using namespace std;

struct PierwiastkiStruct {
	PierwiastkiStruct() :
			pierwiastki(0) {
	}
	void operator()(int x) {
		pierwiastki.push_back(sqrt(x));
	}
	vector<double> pierwiastki;
};

map<char, int> Zliczanie(string tekst) {

	map<char, int> zlicz;

	for (unsigned int i = 0; i < tekst.size(); i++) {
		int z = zlicz.find(tekst[i])->second;
		if (z > 0) {
			z++;
			zlicz[tekst[i]] = z;
		} else {
			zlicz.insert(pair<char, int>(tekst[i], 1));
		}
	}

	return zlicz;
}

vector<double> pierwiastkiKwadratowe(list<int> zakres) {
	PierwiastkiStruct pierwiastki = for_each(zakres.begin(), zakres.end(), PierwiastkiStruct());
	return pierwiastki.pierwiastki;
}

int main() {

	string text = "sasasasaswwwaaaaaaaaaa";
	map<char, int> u = Zliczanie(text);
	for (map<char, int>::iterator it = u.begin(); it != u.end(); ++it) {
		cout << it->first << endl;
		cout << it->second << endl;
	}

	list<int> zakres;
	zakres.push_back(3);
	zakres.push_back(4);
	zakres.push_back(5);
	zakres.push_back(6);
	zakres.push_back(7);
	zakres.push_back(8);
	zakres.push_back(9);
	zakres.push_back(10);
	vector<double> pierwiastki = pierwiastkiKwadratowe(zakres);

	for (vector<double>::iterator it = pierwiastki.begin(); it != pierwiastki.end(); ++it) {
			cout << *it << endl;
		}

	return 0;
}
