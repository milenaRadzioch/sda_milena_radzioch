//9. Napisz funkcj�, kt�ra stworzy wektor przechowuj�cy liczby od 1 do n.
//Nast�pnie utw�rz dwa wektory. Jeden, kt�rzy b�dzie przechowywa� tylko wielokrotno�ci 2,
//a drugi tylko wielokrotno�ci 3. Nast�pnie zwr�� vector przechowuj�cy tylko wielokrotno�ci 2 i 3.
//(remove_copy_if, set_intersection)


#include <map>
#include <list>
#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <math.h>
#include <algorithm>
using namespace std;

struct PierwiastkiStruct {
	PierwiastkiStruct() :
			pierwiastki(0) {
	}
	void operator()(int x) {
		pierwiastki.push_back(sqrt(x));
	}
	vector<double> pierwiastki;
};

map<char, int> Zliczanie(string tekst) {

	map<char, int> zlicz;

	for (unsigned int i = 0; i < tekst.size(); i++) {
		int z = zlicz.find(tekst[i])->second;
		if (z > 0) {
			z++;
			zlicz[tekst[i]] = z;
		} else {
			zlicz.insert(pair<char, int>(tekst[i], 1));
		}
	}

	return zlicz;
}

vector<double> pierwiastkiKwadratowe(list<int> zakres) {
	PierwiastkiStruct pierwiastki = for_each(zakres.begin(), zakres.end(),
			PierwiastkiStruct());
	return pierwiastki.pierwiastki;
}

bool jestPodzielnaPrzez2(int i) {
	return ((i%2)!=0);
}

bool jestPodzielnaPrzez3(int i) {
	return ((i%3)!=0);
}

vector<int> wielokrotnosci(vector<int> liczby) {

	//tworzymy wsp�lny vector dla wielokrtono�ci 2 i 3
	vector<int> wielokrotnosci2i3;

	//tworzymy vector dla wielokrtono�ci 2
	vector<int> liczbyPodzielnePrzez2;
	//ladujemy do vectora liczbyPodzielnePrzez2 liczby podzielne przez 2
	remove_copy_if(liczby.begin(), liczby.end(), inserter(liczbyPodzielnePrzez2,liczbyPodzielnePrzez2.end()), jestPodzielnaPrzez2);

	//tworzymy vector dla wielokrtono�ci 3
	vector<int> liczbyPodzielnePrzez3;
	//ladujemy do vectora liczbyPodzielnePrzez3 liczby podzielne przez 3
	remove_copy_if(liczby.begin(), liczby.end(), inserter(liczbyPodzielnePrzez3,liczbyPodzielnePrzez3.end()), jestPodzielnaPrzez3);

	//ladujemy do vectora wielokrotnosci2i3 vector liczb podzielnych przez 2
	wielokrotnosci2i3.insert(wielokrotnosci2i3.end(), liczbyPodzielnePrzez2.begin(), liczbyPodzielnePrzez2.end());
	//ladujemy do vectora wielokrotnosci2i3 vector liczb podzielnych przez 3
	wielokrotnosci2i3.insert(wielokrotnosci2i3.end(), liczbyPodzielnePrzez3.begin(), liczbyPodzielnePrzez3.end());
	return wielokrotnosci2i3;
}

int main() {

	string text = "sasasasaswwwaaaaaaaaaa";
	map<char, int> u = Zliczanie(text);
	for (map<char, int>::iterator it = u.begin(); it != u.end(); ++it) {
		cout << it->first << endl;
		cout << it->second << endl;
	}

	list<int> zakres;
	zakres.push_back(3);
	zakres.push_back(4);
	zakres.push_back(5);
	zakres.push_back(6);
	zakres.push_back(7);
	zakres.push_back(8);
	zakres.push_back(9);
	zakres.push_back(10);
	vector<double> pierwiastki = pierwiastkiKwadratowe(zakres);

	for (vector<double>::iterator it = pierwiastki.begin();
			it != pierwiastki.end(); ++it) {
		cout << *it << endl;
	}


	//zad 9

	vector<int> liczby;

	//wypelniamy vector liczbami od 1 do 50
	for (int i = 1; i<50; i++) {
		liczby.push_back(i);
	}

	vector<int> podzielnePrzez2 = wielokrotnosci(liczby);

	for (vector<int>::iterator it = podzielnePrzez2.begin();it != podzielnePrzez2.end(); ++it) {
			cout << *it << endl;
	}

	return 0;
}
