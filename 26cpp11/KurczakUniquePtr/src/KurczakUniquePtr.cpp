#include <iostream>
#include <memory>
using namespace std;

struct Kurczak
{
Kurczak()
{
	std::cout << "kurczak " << std::endl;

}
~Kurczak()
{
	std::cout << "~kurczak " << std::endl;
}
};

void fun()
{
	Kurczak* ptr  = new Kurczak();

	delete ptr;
	}

int main() {

	unique_ptr<Kurczak> sprytny(new Kurczak());
	fun();

	return 0;
}
