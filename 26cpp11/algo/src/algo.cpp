#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	vector<int> liczby(201);

	iota(liczby.begin(), liczby.end(), -100);

	for (auto& l : liczby) {
		cout << l << " ";
	}
	cout << endl;

	cout << "Dodatnie: "
			<< all_of(liczby.begin(), liczby.end(), [](int& i) {return i > 0;})
			<< endl;

	cout << "Czy istnieje podzielna przez 3, 5 i 30: "
			<< any_of(liczby.begin(), liczby.end(),
					[](int& i) {return ( (i % 3 == 0) && (i % 5 == 0) && (i % 30 == 0) );})
			<< endl;

	//Super dobrze:
	liczby.erase(remove(liczby.begin(), liczby.end(), 0));
	//Ok
//	auto it = remove(liczby.begin(), liczby.end(), 0);
//	liczby.resize(std::distance(liczby.begin(), it));

	cout << "Czy zadna z liczb nie jest 0: "
			<< none_of(liczby.begin(), liczby.end(),
					[](int& i) {return i == 0;}) << endl;

	cout << "Posortowany rosnaco: " << is_sorted(liczby.begin(), liczby.end())
			<< endl;

	cout << "Posortowany malejaco: "
			<< is_sorted(liczby.begin(), liczby.end(),
					[](int& i1, int& i2) {return i1 > i2;}) << endl;

	vector<int> drugi;
	cout << "Kopia -90 90" << endl;
	copy_if(liczby.begin(), liczby.end(), back_inserter(drugi),
			[](int& i) {return i>90 || i<-90;});
	for (auto& l : drugi) {
		cout << l << " ";
	}

	auto wart78 = find(liczby.begin(), liczby.end(), 78);

	vector<int> trzeci;
	copy_n(wart78, 10, back_inserter(trzeci));
	cout << endl << "10 liczb od 78" << endl;
	for (auto& l : trzeci) {
		cout << l << " ";
	}

	return 0;
}

