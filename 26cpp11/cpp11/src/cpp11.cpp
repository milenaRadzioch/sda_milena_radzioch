#define POTEGA(x) pow(2,x)
#include <cmath>
#include <iostream>
using namespace std;

constexpr int potegadw(int n) {
	return pow(2, n);

}

constexpr int silnia(int m) {

	return m <= 1 ? 1 : (m * silnia(m - 1));
}

int main() {

//	int p1 = POTEGA(1);
//	int p2 = POTEGA(2);
//	int p3 = POTEGA(4);
//	int p4 = POTEGA(7);
//	int p5 = POTEGA(5);

//	int p1 = potegadw(1);
//	int p2 = potegadw(2);
//	int p3 = potegadw(3);
//	int p4 = potegadw(4);
//	int p5 = potegadw(5);

	int p1 = silnia(1);
	int p2 = silnia(2);
	int p3 = silnia(3);

	cout << "p1 = " << p1 << endl;
	cout << "p2 = " << p2 << endl;
	cout << "p3 = " << p3 << endl;
//	cout << "p4 = " << p4 << endl;
//	cout << "p5 = " << p5 << endl;

	return 0;
}
