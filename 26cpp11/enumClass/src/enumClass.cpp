
#include <iostream>
using namespace std;

//enum class - silnie typowany, musi byc przypisywany do innych wartosci enum class ale tego samego typu

enum class KolorPsa
{
	BIALY,
	CZARNY,
	TRIKOLOR
};

enum Figura
{
	kwadrat =0,
	trojkat
};
enum class KolorKota
{
	BIALY,
	CZARNY
};
int main() {

	//KolorPsa pies = BIALY;
	KolorPsa pies = KolorPsa::BIALY;
	KolorKota kot = KolorKota::CZARNY;

	Figura fig = Figura::trojkat;
	cout << fig << endl;
	cout << static_cast<int>(kot) << endl;

	return 0;
}
