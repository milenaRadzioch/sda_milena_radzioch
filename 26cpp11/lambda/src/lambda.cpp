
#include <iostream>
using namespace std;

int main() {

	auto hello = []() {cout << "!!!Hello World!!!" << endl;};

	hello();

	auto dodawanie = [] (int a, int b) -> int {return a+b;};

	int x = dodawanie(3,1);
	cout<< "wynik:" << x <<endl;

	int mnoznik = 5;
	[=](int z) {cout << mnoznik*z;}(3);

}
