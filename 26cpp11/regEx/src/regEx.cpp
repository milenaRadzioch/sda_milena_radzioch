#include <iostream>
#include <regex>
#include <string>
using namespace std;

int main() {
//	string mail = "mielnaradzioch@gmail.com";
//	regex mail_regex("^[a-z]*@[a-z]*.[a-z]{2,3}");
//	//regex mail_regex("~[a-z]*@[a-z]*.[a-z]{2,3}^"); //tylda i daszek tzn od poczatku do konca ma byc taki sam
//	cout<< regex_search(mail, mail_regex)<< endl;
//	cout<< regex_match(mail, mail_regex);

	string mail;
	regex mail_regex("[a-zA-Z0-9]+@[a-zA-Z]+.(com|pl)"); //1. obiekt regex

	while (mail != "42")
	{
		cout << "Podaj mail: ";
		cin >> mail;

		cout << (regex_match(mail, mail_regex) ? "Mail prawid�owy" : "Mail b��dny") << endl;
	}

	return 0;

}
