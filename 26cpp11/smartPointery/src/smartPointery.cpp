#include <iostream>
#include <memory>
#include <string>
#include <vector>
using namespace std;

struct Kurczak {

	string mNazwa;
	Kurczak() :
			Kurczak("") {
		std::cout << "kurczak1 " << std::endl;

	}
	Kurczak(string nazwa) :
			mNazwa(nazwa) {
		std::cout << "kurczak2 " << std::endl;

	}
	~Kurczak() {
		std::cout << "~kurczak " << std::endl;
	}
};

int main() {
	shared_ptr<Kurczak> sprytny = make_shared<Kurczak>();
	shared_ptr<Kurczak> sprytny2 = make_shared<Kurczak>("henio");

	//vector<shared_ptr<Kurczak>> v;
//	v.push_back(sprytny2);
//	v.push_back(sprytny2);
//	v.push_back(sprytny2);
//	v.push_back(sprytny2);
//	v.push_back(sprytny2);

//	cout << "przed" << sprytny2.use_count();
//	sprytny2.reset();
//	cout << "po" << sprytny2.use_count();
//	cout << "v(0)" << sprytny2.use_count();

//	cout << "v(0)" << v[0].use_count() << endl;
//	cout << "przed" << sprytny2.use_count()<< endl;
//	v.clear();
//	cout << "po" << sprytny2.use_count()<<endl;

	return 0;
}
