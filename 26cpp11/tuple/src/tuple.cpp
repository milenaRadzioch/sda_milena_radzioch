#include <iostream>
#include <tuple>
using namespace std;
std::tuple<string,float,int> pobierzPacjenta(int id)
        {
        if(id ==0)
        {
            return make_tuple("Ktos",1.99,56);
        }
        else if(id ==1)
        {
            return make_tuple("Nikt",1.01,12);
        }
        else
        {
            return make_tuple("Blad",0,0);
        }
        }
int main() {
    auto pierwszy = pobierzPacjenta(0);
    auto drugi = pobierzPacjenta(1);
    get<0>(pierwszy) = "Kurczaczek";

    cout<<get<0>(pierwszy)<< " "<<get<1>(pierwszy)<<" "<<get<2>(pierwszy)<<endl;
    string imie;
    float wzrost;
    int waga;
    tie(imie, wzrost, waga) = drugi;
    cout<<imie<<" "<<wzrost<<" "<<waga<<endl;
    return 0;
}
